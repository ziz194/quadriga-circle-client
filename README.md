### Quick start

```bash
# change directory to your app
$ cd quadriga-circle-client

# install the dependencies with npm
$ npm install

# start the server
$ npm run start
```

the app will run on [http://localhost:3000](http://localhost:3000) in your browser.
