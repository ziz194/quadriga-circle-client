/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  // map tells the System loader where to look for things

  var map = {
    'app': 'build/app', // 'dist',
    '@angular': 'node_modules/@angular',
    'angular2-in-memory-web-api': 'node_modules/angular2-in-memory-web-api',
    'angular2-jwt': 'node_modules/angular2-jwt',
    'rxjs': 'node_modules/rxjs',
    '@ngrx': 'node_modules/@ngrx',
    'angular2-localstorage': 'node_modules/angular2-localstorage',
    'ngrx-store-localstorage': 'node_modules/ngrx-store-localstorage',
    'moment': 'node_modules/moment',
    'angular2-moment': 'node_modules/angular2-moment',
    "materialize": "node_modules/materialize-css",
    "angular2-materialize": "node_modules/angular2-materialize",
    "jquery": "node_modules/jquery"

  };
  // packages tells the System loader how to load when no filename and/or no extension
  var packages = {
    'app': { main: 'main.js', defaultExtension: 'js' },
    'rxjs': { defaultExtension: 'js' },
    'angular2-in-memory-web-api': { main: 'index.js', defaultExtension: 'js' },
    'angular2-jwt': { main: 'angular2-jwt.js', defaultExtension: 'js' },
    'angular2-localstorage': { main: 'index.js', defaultExtension: 'js' },
    'angular2-moment': { defaultExtension: 'js' },
    'moment': { main: 'moment.js', defaultExtension: 'js' },
    '@angular/router': { main: 'index.js', defaultExtension: 'js' },
    'ngrx-store-localstorage': { main: '/dist/index.js', defaultExtension: 'js' },
    '@ngrx/core': {
      main: 'index.js',
      format: 'cjs'
    },
    '@ngrx/store': {
      main: 'index.js',
      format: 'cjs'
    },
    'materialize': {
      format: "global",
      main: "dist/js/materialize",
      defaultExtension: "js"
    },
    'angular2-materialize': {
      main: "dist/index",
      defaultExtension: "js"
    }
  };
  var ngPackageNames = [
    'common',
    'compiler',
    'core',
    'http',
    'forms',
    'platform-browser',
    'platform-browser-dynamic',
    'router',
    'router-deprecated',
    'upgrade',
  ];


  // Individual files (~300 requests):
  function packIndex(pkgName) {
    packages['@angular/' + pkgName] = { main: 'index.js', defaultExtension: 'js' };
  }
  // Bundled (~40 requests):
  function packUmd(pkgName) {
    packages['@angular/' + pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
  }
  // Most environments should use UMD; some (Karma) need the individual index files
  var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;
  // Add package entries for angular packages
  ngPackageNames.forEach(setPackageConfig);

  var config = {
    map: map,
    packages: packages
  }
  System.config(config);
})(this);
