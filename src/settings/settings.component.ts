import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { SERVER_URL } from '../config';
import { SITE_TITLE } from '../config';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, provideRouter } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location, NgClass } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState, IQCFaqItems, IQCFaq  } from '../shared/interfaces';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { Title } from '@angular/platform-browser';
import { DOCS_URL } from '../config';
import { SettingsService } from '../shared/setting.service';
import {IQApplicationDefault } from '../shared/interfaces'
import { AuthGuardService } from '../shared/auth.guard.service';

@Component({
    selector: 'div',
    template: require('./settings.html'),
    providers: [SettingsService, AuthGuardService],
    directives: [ROUTER_DIRECTIVES,REACTIVE_FORM_DIRECTIVES, Sidebar, Footer, MaterializeDirective]
})

export class Settings {
    private settingsForm: FormGroup;
    public settingsValues: IQApplicationDefault;

    constructor(public store: Store<AppState>,
        private titleService: Title,
        private formBuilder: FormBuilder,
        public settingsService: SettingsService
        
    ) {
        this.titleService.setTitle(SITE_TITLE); 
        this.createForm();
        this.initDefaults();
    }


     private initDefaults(){
        this.settingsService.findSettings().subscribe(
            data => {
                this.settingsValues = data;
                // //Default form values
                (<FormControl>this.settingsForm.find('default_event_location')).updateValue(this.settingsValues.default_event_location);
                (<FormControl>this.settingsForm.find('default_event_street')).updateValue(this.settingsValues.default_event_street);
                (<FormControl>this.settingsForm.find('default_event_city')).updateValue(this.settingsValues.default_event_city);
                (<FormControl>this.settingsForm.find('default_event_zip')).updateValue(this.settingsValues.default_event_zip);
                (<FormControl>this.settingsForm.find('default_event_number')).updateValue(this.settingsValues.default_event_number);

                (<FormControl>this.settingsForm.find('talk_keywords')).updateValue(this.settingsValues.talk_keywords);
                (<FormControl>this.settingsForm.find('workshop_keywords')).updateValue(this.settingsValues.workshop_keywords);
                (<FormControl>this.settingsForm.find('freizeit_keywords')).updateValue(this.settingsValues.freizeit_keywords);
                (<FormControl>this.settingsForm.find('occupational_areas')).updateValue(this.settingsValues.occupational_areas);
                (<FormControl>this.settingsForm.find('career_levels')).updateValue(this.settingsValues.career_levels);

                // // Mails Content
                (<FormControl>this.settingsForm.find('attendee_confirmation')).updateValue(this.settingsValues.attendee_confirmation);
                (<FormControl>this.settingsForm.find('attendee_confirmation_subject')).updateValue(this.settingsValues.attendee_confirmation_subject);
                (<FormControl>this.settingsForm.find('attendee_event_change')).updateValue(this.settingsValues.attendee_event_change);
                (<FormControl>this.settingsForm.find('attendee_event_change_subject')).updateValue(this.settingsValues.attendee_event_change_subject);
                (<FormControl>this.settingsForm.find('attendee_event_reminder')).updateValue(this.settingsValues.attendee_event_reminder);
                (<FormControl>this.settingsForm.find('attendee_event_reminder_subject')).updateValue(this.settingsValues.attendee_event_reminder_subject);
                (<FormControl>this.settingsForm.find('attendee_rating_reminder')).updateValue(this.settingsValues.attendee_rating_reminder);
                (<FormControl>this.settingsForm.find('attendee_rating_reminder_subject')).updateValue(this.settingsValues.attendee_rating_reminder_subject);
                (<FormControl>this.settingsForm.find('contact')).updateValue(this.settingsValues.contact);
                (<FormControl>this.settingsForm.find('host_event_reminder')).updateValue(this.settingsValues.host_event_reminder);
                (<FormControl>this.settingsForm.find('host_event_reminder_subject')).updateValue(this.settingsValues.host_event_reminder_subject);
                (<FormControl>this.settingsForm.find('host_rating_reminder')).updateValue(this.settingsValues.host_rating_reminder);
                (<FormControl>this.settingsForm.find('host_rating_reminder_subject')).updateValue(this.settingsValues.host_rating_reminder_subject);
                (<FormControl>this.settingsForm.find('contact_subject')).updateValue(this.settingsValues.contact_subject);               
                (<FormControl>this.settingsForm.find('contact_reply')).updateValue(this.settingsValues.contact_reply);
                (<FormControl>this.settingsForm.find('contact_reply_subject')).updateValue(this.settingsValues.contact_reply_subject);
                (<FormControl>this.settingsForm.find('event_delete')).updateValue(this.settingsValues.event_delete);
                (<FormControl>this.settingsForm.find('event_delete_subject')).updateValue(this.settingsValues.event_delete_subject);             
                (<FormControl>this.settingsForm.find('host_attendee_info')).updateValue(this.settingsValues.host_attendee_info);
                (<FormControl>this.settingsForm.find('host_attendee_info_subject')).updateValue(this.settingsValues.host_attendee_info_subject);
                (<FormControl>this.settingsForm.find('host_attendee_limit')).updateValue(this.settingsValues.host_attendee_limit);
                (<FormControl>this.settingsForm.find('host_attendee_limit_subject')).updateValue(this.settingsValues.host_attendee_limit_subject);               
                (<FormControl>this.settingsForm.find('password')).updateValue(this.settingsValues.password);
                (<FormControl>this.settingsForm.find('password_subject')).updateValue(this.settingsValues.password_subject);
                (<FormControl>this.settingsForm.find('user_registration')).updateValue(this.settingsValues.user_registration);
                (<FormControl>this.settingsForm.find('user_registration_subject')).updateValue(this.settingsValues.user_registration_subject);
                (<FormControl>this.settingsForm.find('create_event_mail_confirmation_text')).updateValue(this.settingsValues.create_event_mail_confirmation_text);
                (<FormControl>this.settingsForm.find('create_event_mail_confirmation_subject')).updateValue(this.settingsValues.create_event_mail_confirmation_subject);
                (<FormControl>this.settingsForm.find('user_approved')).updateValue(this.settingsValues.user_approved);
                (<FormControl>this.settingsForm.find('user_approved_subject')).updateValue(this.settingsValues.user_approved_subject);
                (<FormControl>this.settingsForm.find('attendee_abmelden')).updateValue(this.settingsValues.attendee_abmelden);
                (<FormControl>this.settingsForm.find('attendee_abmelden_subject')).updateValue(this.settingsValues.attendee_abmelden_subject);
                (<FormControl>this.settingsForm.find('new_comment')).updateValue(this.settingsValues.new_comment);
                (<FormControl>this.settingsForm.find('new_comment_subject')).updateValue(this.settingsValues.new_comment_subject);

                // Mail Settings

                (<FormControl>this.settingsForm.find('mail_html_template')).updateValue(this.settingsValues.mail_html_template);
                (<FormControl>this.settingsForm.find('mail_from')).updateValue(this.settingsValues.mail_from);
                (<FormControl>this.settingsForm.find('mail_from_descr')).updateValue(this.settingsValues.mail_from_descr);
                (<FormControl>this.settingsForm.find('reply_to')).updateValue(this.settingsValues.reply_to);
            }
        );

    }

        createForm() {

        this.settingsForm = this.formBuilder.group({
            // Mail Settings
            mail_html_template: ['', Validators.required],
            mail_from:['', Validators.required],
            mail_from_descr:['', Validators.required],
            reply_to:['', Validators.required],
            // Mails Content
            attendee_confirmation:['', Validators.nullValidator],
            attendee_confirmation_subject:['', Validators.nullValidator],
            attendee_event_change:['', Validators.nullValidator],
            attendee_event_change_subject:['', Validators.nullValidator],
            attendee_event_reminder:['', Validators.nullValidator],
            attendee_event_reminder_subject:['', Validators.nullValidator],
            attendee_rating_reminder:['', Validators.nullValidator],
            attendee_rating_reminder_subject:['', Validators.nullValidator],
            host_event_reminder:['', Validators.nullValidator],
            host_event_reminder_subject:['', Validators.nullValidator],
            host_rating_reminder:['', Validators.nullValidator],
            host_rating_reminder_subject:['', Validators.nullValidator],
            contact:['', Validators.nullValidator],
            contact_subject:['', Validators.nullValidator],
            contact_reply:['', Validators.nullValidator],
            contact_reply_subject:['', Validators.nullValidator],
            event_delete:['', Validators.nullValidator],
            event_delete_subject:['', Validators.nullValidator],
            host_attendee_info:['', Validators.nullValidator],
            host_attendee_info_subject:['', Validators.nullValidator],
            host_attendee_limit:['', Validators.nullValidator],
            host_attendee_limit_subject:['', Validators.nullValidator],
            password:['', Validators.nullValidator],
            password_subject:['', Validators.nullValidator],
            user_registration:['', Validators.nullValidator],
            user_registration_subject:['', Validators.nullValidator],
            create_event_mail_confirmation_text:['', Validators.nullValidator],
            create_event_mail_confirmation_subject:['', Validators.nullValidator],
            user_approved:['', Validators.nullValidator],
            user_approved_subject:['', Validators.nullValidator],
            attendee_abmelden:['', Validators.nullValidator],
            attendee_abmelden_subject:['', Validators.nullValidator],
            new_comment:['', Validators.nullValidator],
            new_comment_subject:['', Validators.nullValidator],
            
            // Default form Values
            default_event_location:['', Validators.required],
            default_event_city:['', Validators.required],
            default_event_zip:['', Validators.required],
            default_event_street:['', Validators.required],
            default_event_number:['', Validators.required],   
            talk_keywords:['', Validators.required],
            workshop_keywords:['', Validators.required],
            freizeit_keywords:['', Validators.required],
            occupational_areas:['', Validators.required],
            career_levels:['', Validators.required]

        });

    }


        onSubmit(): void {

        if (this.settingsForm.valid) {
            
            Materialize.toast('Die Einstellungen werden gespeichert...', 4000);
            let multipartform = new FormData();

            this.settingsService.updateSettings(this.settingsForm.value, multipartform).then(
                result => {
                    Materialize.toast('Die Einstellungen wurden gespeichert...', 4000, 'green');
                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red')
                    return false;
                }
            );


        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }

    }


}