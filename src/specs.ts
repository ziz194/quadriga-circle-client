//import 'systemjs';

import 'es6-shim';
import 'reflect-metadata';
import 'core-js';
import 'zone.js/dist/zone';

import './shared/auth.service.spec.ts';


/*
function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

requireAll(require.context('./shared', true, /\.spec\.ts$/));
*/