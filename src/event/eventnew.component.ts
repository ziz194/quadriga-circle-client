import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile, IQApplicationDefault } from '../shared/interfaces'
import { Store } from '@ngrx/store';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { EventService } from '../shared/event.service';
import { SettingsService } from '../shared/setting.service';
import { IMAGE_URL } from '../config'
import { base64Eencode } from '../shared/imagereader';
import { SERVER_URL} from '../config';
import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { MyUploader} from "../profile/myuploader";
import { AuthGuardService } from '../shared/auth.guard.service';

const API_URL = SERVER_URL + 'uploadFile';
@Component({
    selector: 'div',
    template: require('./eventnew.html'),
    directives: [FILE_UPLOAD_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    providers: [EventService, SettingsService, AuthGuardService]
})


export class EventNew {

    private eventForm: FormGroup;
    private IMAGE_URL = IMAGE_URL;
    private previewPicPath: string; 
    private event: IQCEvent;
    public viewstep: number = 1;
    public repeat: string = "none";
    public page_mode: string = "new";
    public dataLoaded: boolean = true;
    public defaultValues: IQApplicationDefault;
    public freizeit_keywords: string[];
    public talk_keywords: string[];
    public workshop_keywords: string[];
    public confirmShow: boolean = false ; 
    private photoIsValid = true ; 
    private queueItem = 0 ;
    private docsQueueItem = 0;
    private repeatDays: string[] = [];
    private customTopics: string[] = [];

    public uploader: MyUploader = new MyUploader({ url: API_URL });
    public docsUploader: MyUploader = new MyUploader({ url: API_URL });
    
    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public eventService: EventService,
        public settingService: SettingsService,
        private router: Router
    ) {

        this.repeatDays.length;
        this.createForm();
        this.initDefaults();

    }
   

    private initDefaults(){
        
        this.customTopics = [];
        this.settingService.findSettings().subscribe(
            data => {
                this.defaultValues = data;
                if (this.defaultValues.freizeit_keywords) {
                    this.freizeit_keywords = this.defaultValues.freizeit_keywords.split(",");
                    this.freizeit_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.workshop_keywords) {
                    this.workshop_keywords = this.defaultValues.workshop_keywords.split(",");
                    this.workshop_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.talk_keywords) {
                    this.talk_keywords = this.defaultValues.talk_keywords.split(",");
                    this.talk_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }

                (<FormControl>this.eventForm.find('host_company')).updateValue(this.defaultValues.default_event_location);
                (<FormControl>this.eventForm.find('host_street')).updateValue(this.defaultValues.default_event_street);
                (<FormControl>this.eventForm.find('host_city')).updateValue(this.defaultValues.default_event_city);
                (<FormControl>this.eventForm.find('host_zip')).updateValue(this.defaultValues.default_event_zip);
                (<FormControl>this.eventForm.find('host_house_number')).updateValue(this.defaultValues.default_event_number);
            }
        );

    }

    ngOnInit() {


    }

    public enableConfirm(): void {
        this.confirmShow = true ;
        $('#copyright_confirm_event').prop('disabled', false);
        $('#copyright_confirm_event').prop('checked', false);

    }

    public checkPhoto(): void {
        if ($('#eventAvatarFile').val() == '')  {
            this.photoIsValid = true ; 
            this.confirmShow = false ;
            $('#copyright_confirm_event').prop('checked', true);
        }
    }

    public disableConfirm(): void {
        $('#copyright_confirm_event').prop('disabled', true);

    }

    changeEventType(){

        this.customTopics = [];

    }

    uploadEventPicTemp() {
        // Set uploader description
        this.uploader.onBuildItemForm = (item, form) => {
            form.append("description", 'temp');
        };
        this.uploader.queue.forEach(item => {
            if (item.file.size > 3000000){
                Materialize.toast('Das Bild ist zu groß', 4000, 'red');
                this.uploader.clearQueue();
            }                
            else{
                item.upload(); 
                this.uploader.onSuccessItem = (item:any, response:any, status:any, headers:any) => {
                    if (response == 'NO'){
                        Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus oder das Bild feld leeren', 4000, 'red');
                        this.photoIsValid = false
                        this.uploader.clearQueue();
                    }
                    else{
                        this.photoIsValid = true;
                         // Event picture preview
                        $( "#previewPicOverlay" ).removeClass('orange')
                        $( "#previewPicOverlay" ).animate({
                            height: "220"
                        }, 200, function() {
                        });
                        $( "#previewPic" ).animate({
                            opacity: 0.6,
                            color: "#f57c00",
                            height: "220"
                        }, 200, function() {
                        });
                        this.previewPicPath = IMAGE_URL+'tmp/'+response;
                        // $('#previewPic').css('background-image','url('+IMAGE_URL+'tmp/tmp.jpg') ;
                        $('#previewPic').html('') ;
                    }
               
                }
                this.queueItem = this.uploader.getIndexOfItem(item);
            }
        });

    }

    uploadEventPic() {
        this.uploader.queue[this.queueItem].upload();
        Materialize.toast('Das Eventbild ist hochgeladen!', 4000, 'green');
        $('#copyright_confirm_event').prop('disabled', true);
    }

    uploadEventDocs() {
        this.docsUploader.uploadAll();
    }

    prevPage() {
        this.switchPage(false);
    }

    nextPage() {
        if(this.viewstep == 2){

            if($('#copyright_confirm_event').prop('checked') != true){
                    Materialize.toast('Bitte Bestätigen Sie dass Sie alle Rechte vom hochgeladenen Bild Haben', 4000, 'red');
                    return;
            }
            

        }     
        this.switchPage(true);
    
    }

    switchPage(forward: boolean) {

        var formBlockvalid: boolean = false;
        if(forward){
            switch (this.viewstep) {
                case 1:
                    if (this.eventForm.controls['type'].valid &&
                        this.eventForm.controls['min_attendees'].valid &&
                        this.eventForm.controls['max_attendees'].valid
                    ) {
                        formBlockvalid = true;
                    }
                    break;
                case 2:
                    if (this.eventForm.controls['title'].valid &&
                        this.eventForm.controls['description'].valid &&
                        this.photoIsValid
                    ) {
                        formBlockvalid = true;
                    }
                    break;
                case 3:
                    if (this.eventForm.controls['start_date'].valid &&
                        this.eventForm.controls['start_time'].valid &&
                        this.eventForm.controls['end_time'].valid &&
                        this.eventForm.controls['repeat'].valid
                    ) {
                        formBlockvalid = true;
                    }
                    break;
            }

            if (formBlockvalid) {
                    this.viewstep++;

            }
            else {
                if (!this.eventForm.controls['title'].valid){
                    Materialize.toast('Bitte vervollständige das Feld Eventtitel', 4000, 'red');
                }
                else if (!this.eventForm.controls['description'].valid){
                    Materialize.toast('Bitte vervollständige das Feld Kurzbeschreibung', 4000, 'red');
                }
                else if (!this.photoIsValid){
                    Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus oder das Bild feld leeren', 4000, 'red');
                }
                else Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
            }
        }
        else {
            this.viewstep--;
        }
    }

    clearForm() {

        this.dataLoaded = true; // form reset only possible until angular RC5
        this.viewstep = 1;
        (<FormControl>this.eventForm.find('title')).updateValue('');
        (<FormControl>this.eventForm.find('description')).updateValue('');
        (<FormControl>this.eventForm.find('start_date')).updateValue('');
        (<FormControl>this.eventForm.find('start_time')).updateValue('08:00');
        (<FormControl>this.eventForm.find('end_time')).updateValue('16:00');
        (<FormControl>this.eventForm.find('repeat')).updateValue('none');
        (<FormControl>this.eventForm.find('host_company')).updateValue(this.defaultValues.default_event_location);
        (<FormControl>this.eventForm.find('host_street')).updateValue(this.defaultValues.default_event_street);
        (<FormControl>this.eventForm.find('host_city')).updateValue(this.defaultValues.default_event_city);
        (<FormControl>this.eventForm.find('host_zip')).updateValue(this.defaultValues.default_event_zip);
        (<FormControl>this.eventForm.find('host_house_number')).updateValue(this.defaultValues.default_event_number);
        this.uploader.clearQueue();
        this.docsUploader.clearQueue();
    }

    createForm() {

        this.eventForm = this.formBuilder.group({
            type: ['talk', Validators.required],
            min_attendees: ['6', Validators.required],
            max_attendees: ['12', Validators.required],

            title: ['', Validators.required],
            description: ['', Validators.required],

            start_date: ['', Validators.required],
            start_time: ['08:00', Validators.required],
            end_time: ['16:00', Validators.required],
            repeat: ['none', Validators.required],
            repeat_end_daily: ['', Validators.nullValidator],
            repeat_cycle_weekly: ['', Validators.nullValidator],
            repeat_end_weekly: ['', Validators.nullValidator],
            repeat_day_weekly: ['', Validators.nullValidator],
            repeat_cycle_monthly: ['', Validators.nullValidator],
            repeat_end_monthly: ['', Validators.nullValidator],
            repeat_day_monthly: ['', Validators.nullValidator],
            repeat_cycle_yearly: ['', Validators.nullValidator],
            repeat_end_yearly: ['', Validators.nullValidator],
            repeat_day_yearly: ['', Validators.nullValidator],
            repeat_custom: ['', Validators.nullValidator],
            host_company: ['', Validators.required],
            host_street: ['', Validators.required],
            custom_days: ['', Validators.nullValidator],
            topic: ['', Validators.nullValidator],
            host_house_number: ['13', Validators.required],
            host_zip: ['', Validators.required],
            host_city: ['', Validators.required],
            host_details: ['', Validators.nullValidator]

        });

    }

    addDay(e) {

        this.repeatDays.push( this.eventForm.find('custom_days').value );
        this.repeatDays = this.repeatDays.filter(function(x){
            return (x !== (undefined || '' || typeof(undefined) ));
        });
    }

    toogleTopic(e) {
        
        let value:string = e.target.id;

        if (e.target.checked){
            // value = value.replace('tag_','');
            this.customTopics.push( value );
        } else {
              this.customTopics.splice( this.customTopics.indexOf(value) );
        }
   
    }

    removeDay(index){
        
        this.repeatDays.splice(index);
    }

    onSubmit(): void {

        if (this.eventForm.valid) {
            // if($('#eventAvatar').val() === "" ){
            //     Materialize.toast('Bitte wähle ein Bild aus', 4000, 'red');
            //     this.viewstep = 2 ; 
            // }
            // else{ 
            
            Materialize.toast('Dein Event wird erstellt...', 4000);

            let multipartform = new FormData();

            (<FormControl>this.eventForm.find('repeat_custom')).updateValue(this.repeatDays.join(','));
            (<FormControl>this.eventForm.find('topic')).updateValue(this.customTopics.join(','));
            this.eventService.insertOne(this.eventForm.value, multipartform).then(
                result => {

                    if($('#eventDocuments').val() !="" ){
                        this.docsUploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventDocument');
                        };
                        this.uploadEventDocs();
                        this.docsQueueItem = 0;
                    }
                    this.dataLoaded = false;
                    if(this.uploader.getReadyItems.length == 0){
                        this.uploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventPhoto');
                        };
                        this.uploadEventPic();
                    }
               
      
                    jQuery('#modal1').openModal();
                    this.initDefaults();
                    Materialize.toast('Dein Event wurde erstellt', 4000, 'green');
                    }


            ).catch(
                error => {
                    jQuery('#modal1').openModal();
                    Materialize.toast('Dein Event wurde erstellt', 4000, 'green');
                    // Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red')
                    // return false;
                }
            );
        
            }

        else {
            if (!this.eventForm.controls['start_date'].valid){
                Materialize.toast('Bitte gib ein Datum ein', 4000, 'red');
            }
            else{ Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red'); }
        }

    }

}
