import { Component } from '@angular/core';
import { SERVER_URL } from '../config';
import { SITE_TITLE } from '../config';
import { EventService } from '../shared/event.service';
import { ProfileService } from '../shared/profile.service';
import { Title } from '@angular/platform-browser';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location, NgClass } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState, IQCEvent, IQCEventItems, IQCBookmarks, IQCUserProfile  } from '../shared/interfaces';
import { SETPROSPOSAL, SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL, ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_PROSPOSAL } from '../shared/state.actions';
import { DateFormatPipe } from '../../node_modules/angular2-moment/DateFormatPipe';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch } from '../shared/pipe.filters';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { PaginatePipe, PaginationControlsCmp, PaginationService } from 'ng2-pagination';
import { IMAGE_URL } from '../config'
import { AuthGuardService } from '../shared/auth.guard.service';

@Component({
    selector: "div",
    template: require('./eventlist.html'),
    providers: [EventService, PaginationService, AuthGuardService],
    directives: [ROUTER_DIRECTIVES, NgClass, Sidebar, Footer, Header, MaterializeDirective, PaginationControlsCmp],
    pipes: [PaginatePipe, FilterPipe, CapitalizePipe, DateFormatPipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch]
})

export class MarketplaceList {

    public events_error: Boolean = false;
    public qceventitems: Observable<IQCEventItems>;
    public event: Observable<IQCEvent>;
    public qceventbookmarks: Observable<IQCBookmarks>;
    private IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;
    private profile: IQCUserProfile;
    public topicSearch = '';
    public typeSearch = '';
    public events;
    public currentbookmarks;
    public topicSearchDefault: string = 'Alle Themen';
    public typeSearchDefault: string = 'Alle Arten';


    public start_date_search: string = '';
    public end_date_search: string = '';

    constructor(public store: Store<AppState>,
        public eventService: EventService,
        private profileService: ProfileService,
        private titleService: Title,
        private router: Router,
        private location: Location
    ) {
        
        this.qceventitems = store.select<IQCEventItems>('qceventprosposals');
        this.titleService.setTitle(SITE_TITLE);

        this.qceventbookmarks = store.select<IQCBookmarks>('qceventbookmarks');
        this.qceventbookmarks.subscribe(
            data => {
                this.currentbookmarks = data;
            }
        );
        this.store.select<IQCUserProfile>('qcuserprofile').subscribe(
            data => {
                this.profile = data;
            }
        )
        this.getEvents();
    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    private getEvents() {

        this.eventService.findPrososal().subscribe(
            data => {
                this.store.dispatch({ type: SETPROSPOSAL, payload: data });;
            },
            err => { console.log(err) }
        );

    }

    filterTopic(search) {
        this.topicSearch = search;
        if (this.topicSearch === '') {
            this.topicSearchDefault = 'Alle Themen';
        } else {
            this.topicSearchDefault = search;
        }
    }

    filterType(search) {
        this.typeSearch = search;
        if (this.typeSearch === '') {
            this.typeSearchDefault = 'Alle Arten';
        } else {
            this.typeSearchDefault = search;
        }
    }

    filterDate() {

        let start_date_search_default = "01.01.1985" ;
        let end_date_search_default = '01.01.2085' ;
        let start_date_send = "" ;
        let end_date_send = "" ; 

        if(this.start_date_search == ''){
            start_date_send = start_date_search_default ; 
        }
        else start_date_send = this.start_date_search ; 

        if(this.end_date_search == ''){
            end_date_send   = '01.01.2085' ; 
        }
        else end_date_send = this.end_date_search ; 
        this.eventService.findAllByDate(start_date_send,end_date_send).subscribe(
                data => {
                    this.store.dispatch({ type: SETPROSPOSAL, payload: data });;
                },
                err => { console.log(err) }
            );
   
        
    }

    toggleBookmark(event: IQCEvent) {

        if (event.isBookmarked) {
            this.store.dispatch({ type: DEL_EVENT_BOOKMARK, payload: event.id });
            this.profileService.delBookmark(this.profile.id, event.id);
        } else {
            this.profileService.addBookmark(this.profile.id, event.id);
            this.store.dispatch({ type: ADD_EVENT_BOOKMARK, payload: event.id });
        }


        event.isBookmarked = !event.isBookmarked;
        this.store.dispatch({ type: UPDATE_PROSPOSAL, payload: event });

    }

    pageChanged($event): void {

        window.scrollTo(0, 0);

    }

}