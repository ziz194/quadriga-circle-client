import { Component, OnInit } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile, IQApplicationDefault } from '../shared/interfaces'
import { Store } from '@ngrx/store';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { EventService } from '../shared/event.service';
import { SettingsService } from '../shared/setting.service';
import { IMAGE_URL } from '../config'
import { base64Eencode } from '../shared/imagereader';
import { Title } from '@angular/platform-browser';
import { MyUploader} from "../profile/myuploader";
import { SERVER_URL} from '../config';
import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { AuthGuardService } from '../shared/auth.guard.service';



import { SETEVENT, DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM, SET_CURRENT_USER_PROFILE } from '../shared/state.actions'
import * as moment from 'moment';

const API_URL = SERVER_URL + 'uploadFile';
@Component({
    selector: 'div',
    template: require('./eventnew.html'),
    directives: [FILE_UPLOAD_DIRECTIVES,REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    providers: [EventService, SettingsService, AuthGuardService]
})

export class EventEdit {

    private eventForm: FormGroup;
    private IMAGE_URL = IMAGE_URL;
    public event: Observable<IQCEvent>;
    public viewstep: number = 1;
    public repeat: string = "none";
    public uploader: MyUploader = new MyUploader({ url: API_URL });
    public docsUploader: MyUploader = new MyUploader({ url: API_URL });
    public currentevent: IQCEvent;
    public page_mode: string = "edit";
    public dataLoaded: boolean = false;
    public freizeit_keywords: string[];
    public talk_keywords: string[];
    public workshop_keywords: string[];
    private queueItem = 0;
    public defaultValues: IQApplicationDefault;
    private docsQueueItem = 0;
    public confirmShow: boolean = false ; 
    private photoIsValid = true ; 
    private previewPicPath: string ; 
    private repeatDays: string[] = [];
    private customTopics: string[] = [];


    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public eventService: EventService,
        public settingService: SettingsService,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title
        ) {
        this.initDefaults();
        this.repeatDays.length;
        this.createForm();


        

    }

        private initDefaults(){
        
        this.customTopics = [];
        this.settingService.findSettings().subscribe(
            data => {
                this.defaultValues = data;
                if (this.defaultValues.freizeit_keywords) {
                    this.freizeit_keywords = this.defaultValues.freizeit_keywords.split(",");
                    this.freizeit_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.workshop_keywords) {
                    this.workshop_keywords = this.defaultValues.workshop_keywords.split(",");
                    this.workshop_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.talk_keywords) {
                    this.talk_keywords = this.defaultValues.talk_keywords.split(",");
                    this.talk_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                        
                    });
                }

            this.route.params.subscribe(params => {
            this['id'] = +params['id'];
            this.eventService.findOne(this['id'])
                .subscribe(
                data => {
                    this.event = data;
                    // Intialise the checkboxes
                    if (data['topic'] !== '') {
                        for (var item of data['topic'].split(',')) {
                            if (this.eventForm.find(item)) {
                                console.log('Schlagwort: '+item) ;
                                (<FormControl>this.eventForm.find(item)).updateValue(1);
                                this.customTopics.push(item);
                            }
                        }
                    }
                });
            }
                  
        );

            this.route.params.subscribe(params => {
            this['id'] = +params['id'];
            this.eventService.findOne(this['id'])
                .subscribe(
                data => {
                    this.event = data;

                    this.store.dispatch({ type: SETEVENT, payload: data })
                    this.titleService.setTitle(this.event['title']);
                    this.dataLoaded = true;
                    let date = moment(this.event['time']);
                    let enddate = moment(this.event['endtime']);

                    (<FormControl>this.eventForm.find('type')).updateValue(this.event['type']);
                    (<FormControl>this.eventForm.find('title')).updateValue(this.event['title']);
                    (<FormControl>this.eventForm.find('description')).updateValue(this.event['description']);
                    (<FormControl>this.eventForm.find('min_attendees')).updateValue(this.event['min_attendees']);
                    (<FormControl>this.eventForm.find('max_attendees')).updateValue(this.event['max_attendees']);
                    (<FormControl>this.eventForm.find('host_company')).updateValue(this.event['host_company']);
                    (<FormControl>this.eventForm.find('host_street')).updateValue(this.event['host_street']);
                    (<FormControl>this.eventForm.find('host_house_number')).updateValue(this.event['host_house_number']);
                    (<FormControl>this.eventForm.find('host_zip')).updateValue(this.event['host_zip']);
                    (<FormControl>this.eventForm.find('host_city')).updateValue(this.event['host_city']);
                    (<FormControl>this.eventForm.find('host_details')).updateValue(this.event['host_details']);
                    (<FormControl>this.eventForm.find('repeat')).updateValue('none');
                    (<FormControl>this.eventForm.find('start_date')).updateValue(date.format('D.MM.YYYY'));
                    (<FormControl>this.eventForm.find('start_time')).updateValue(date.format('HH:mm'));
                    (<FormControl>this.eventForm.find('end_time')).updateValue(enddate.format('HH:mm'));

                    this.eventForm.markAsTouched;
                    this.eventForm.updateValueAndValidity(true);

                    },
                    err => { console.log('eventdetail error'); this['events_error'] = true }
                    )
        });

    })
        }

    ngOnInit() {
        this.event = this.store.select<IQCEvent>('event');
    }


    public enableConfirm(): void {
        this.confirmShow = true ;
        $('#copyright_confirm_event').prop('disabled', false);
        $('#copyright_confirm_event').prop('checked', false);

    }
    public checkPhoto(): void {
        if ($('#eventAvatarFile').val() == '')  {
            this.photoIsValid = true ; 
            this.confirmShow = false ;
            $('#copyright_confirm_event').prop('checked', true);
        }
    }

    public disableConfirm(): void {
        $('#copyright_confirm_event').prop('disabled', true);
    }

    changeEventType(){
        this.customTopics = [];
    }

    uploadEventPicTemp() {
        // Set uploader description
        this.uploader.onBuildItemForm = (item, form) => {
            form.append("description", 'temp');
        };
        this.uploader.queue.forEach(item => {
            if (item.file.size > 3000000){
                Materialize.toast('Das Bild ist zu groß, bitte wähle ein anderes Bild aus', 4000, 'red');
                this.uploader.clearQueue();
            }                
            else{
                item.upload(); 
                this.uploader.onSuccessItem = (item:any, response:any, status:any, headers:any) => {
                    if (response == 'NO'){
                        Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus', 4000, 'red');
                        this.uploader.clearQueue();
                        this.photoIsValid = false
                    }
                    else{
                        this.photoIsValid = true;
                        // Event picture preview
                        $( "#previewPicOverlay" ).removeClass('orange')
                        $( "#previewPicOverlay" ).animate({
                            height: "220"
                        }, 2000, function() {
                        });
                        $( "#previewPic" ).animate({
                            opacity: 0.6,
                            color: "#f57c00",
                            height: "220"
                        }, 2000, function() {
                        });
                        this.previewPicPath = IMAGE_URL+'tmp/'+response
                        // $('#previewPic').css('background-image','url('+IMAGE_URL+'tmp/tmp.jpg') ;
                        $('#previewPic').html('') ;
                    }
                }
                this.queueItem = this.uploader.getIndexOfItem(item);
            }
        });
    }

    uploadEventPic() {
        this.uploader.queue[this.queueItem].upload();
        Materialize.toast('Das Eventbild ist hochgeladen!', 4000, 'green');
        $('#copyright_confirm_event').prop('disabled', true);

    }

    uploadEventDocs() {
        this.docsUploader.uploadAll();
    }


    prevPage(){
        this.switchPage(false);
    }

    nextPage(){
        this.switchPage(true);
    }

    switchPage(forward:boolean){

        var formBlockvalid:boolean = false;

        switch(this.viewstep) {
            case 1:
                if (this.eventForm.controls['type'].valid &&
                    this.eventForm.controls['min_attendees'].valid &&
                    this.eventForm.controls['max_attendees'].valid
                ) {
                    formBlockvalid = true;
                }
                break;
            case 2:
                if (this.eventForm.controls['title'].valid &&
                    this.eventForm.controls['description'].valid &&
                    this.photoIsValid
                ) {
                    formBlockvalid = true;
                }
                break;
            case 3:
                if (this.eventForm.controls['start_date'].valid &&
                    this.eventForm.controls['start_time'].valid &&
                    this.eventForm.controls['end_time'].valid &&
                    this.eventForm.controls['repeat'].valid
                ) {
                    formBlockvalid = true;
                }
                break;
        }

        if (formBlockvalid) {
            if (forward) {
                this.viewstep++;
            } 
            else {
                this.viewstep--;
            }
        }
        else {
            if (!this.eventForm.controls['title'].valid){
                Materialize.toast('Bitte vervollständige das Feld Eventtitel', 4000, 'red');
            }
            else if (!this.eventForm.controls['description'].valid){
                Materialize.toast('Bitte vervollständige das Feld Kurzbeschreibung', 4000, 'red');
            }
            else if (!this.photoIsValid){
                Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus oder das Bild feld leeren', 4000, 'red');
            }

            else Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');        }
    }

    createForm() {
        //date.add(2, 'days');

        this.eventForm = this.formBuilder.group({
            type: ['', Validators.required],
            min_attendees: ['', Validators.required],
            max_attendees: ['', Validators.required],

            title: ['', Validators.required],
            description: ['', Validators.required],
            start_date: ['', Validators.required],
            start_time: ['', Validators.required],
            end_time: ['', Validators.required],
            repeat: ['', Validators.required],
            repeat_end_daily: ['', Validators.nullValidator],
            repeat_cycle_weekly: ['', Validators.nullValidator],
            repeat_end_weekly: ['', Validators.nullValidator],
            repeat_day_weekly: ['', Validators.nullValidator],
            repeat_cycle_monthly: ['', Validators.nullValidator],
            repeat_end_monthly: ['', Validators.nullValidator],
            repeat_day_monthly: ['', Validators.nullValidator],
            repeat_cycle_yearly: ['', Validators.nullValidator],
            repeat_end_yearly: ['', Validators.nullValidator],
            repeat_day_yearly: ['', Validators.nullValidator],
            repeat_custom: ['', Validators.nullValidator],
            host_company: ['', Validators.required],
            host_street: ['', Validators.required],
            custom_days: ['', Validators.nullValidator],
            topic: ['', Validators.nullValidator],
            host_house_number: ['', Validators.required],
            host_zip: ['', Validators.required],
            host_city: ['', Validators.required],
            host_details: ['', Validators.nullValidator]
        });

    }


    addDay(e) {

        this.repeatDays.push( this.eventForm.find('custom_days').value );
        this.repeatDays = this.repeatDays.filter(function(x){
            return (x !== (undefined || '' || typeof(undefined) ));
        });
    }

    toogleTopic(e) {
        
        let value:string = e.target.id;

        if (e.target.checked){
            this.customTopics.push( value );
        } else {
              this.customTopics.splice( this.customTopics.indexOf(value) );
        }
   
    }

    removeDay(index){
        
        this.repeatDays.splice(index);
    }

    onSubmit(): void {

        if (this.eventForm.valid) {
            Materialize.toast('Das Event wird gespeichert...', 4000);
            let multipartform = new FormData();
            (<FormControl>this.eventForm.find('repeat_custom')).updateValue(this.repeatDays.join(','));
            (<FormControl>this.eventForm.find('topic')).updateValue(this.customTopics.join(','));
            this.eventService.updateOne(this.event['id'], this.eventForm.value, multipartform).then(
                result => {
                    this.dataLoaded = false;
                    if(this.uploader.getReadyItems.length == 0){
                        this.uploader.onBuildItemForm = (item, form) => {
                            form.append("event", this.event['id']);
                            form.append("description", 'eventPhoto');
                        };
                        this.uploadEventPic();
                    }
                    if($('#eventDocuments').val() !="" ){
                        this.docsUploader.onBuildItemForm = (item, form) => {
                            form.append("event", this.event['id']);
                            form.append("description", 'eventDocument');
                        };
                        this.uploadEventDocs();
                        this.docsQueueItem = 0;
                    }
                    Materialize.toast('Das Event wurde gespeichert.', 4000, 'green');
                    this.initDefaults();
                    this.router.navigate(['/events']);
                }

            ).catch(
                  error =>  {
                    jQuery('#modal1').openModal();
                    Materialize.toast('Dein Event wurde erstellt', 4000, 'green');
                    //   Materialize.toast('Ein Fehler ist aufgeteten.' , 4000, 'red')
                    //   return false;
                }
            );   
        } else {
            if (!this.eventForm.controls['start_date'].valid){
                Materialize.toast('Bitte gib ein Datum ein', 4000, 'red');
            }
            else{ Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red'); }
        }
        
    }
 
}
