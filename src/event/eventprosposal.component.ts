import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile, IQApplicationDefault } from '../shared/interfaces'
import { Store } from '@ngrx/store';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { EventService } from '../shared/event.service';
import { SettingsService } from '../shared/setting.service';
import { IMAGE_URL } from '../config'
import { base64Eencode } from '../shared/imagereader';
import { SERVER_URL} from '../config';
import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { MyUploader} from "../profile/myuploader";
import { AuthGuardService } from '../shared/auth.guard.service';




const API_URL = SERVER_URL + 'uploadFile';
@Component({
    selector: 'div',
    template: require('./eventprosposal.html'),
    directives: [FILE_UPLOAD_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective, AuthGuardService],
    providers: [EventService, SettingsService, AuthGuardService]
})


export class EventProsposal {

    private eventForm: FormGroup;
    private IMAGE_URL = IMAGE_URL;
    private event: IQCEvent;
    public viewstep: number = 1;
    public repeat: string = "none";
    public page_mode: string = "new";
    public dataLoaded: boolean = true;
    public defaultValues: IQApplicationDefault;
    public freizeit_keywords: string[];
    public talk_keywords: string[];
    public workshop_keywords: string[];
    public confirmShow: boolean = false ; 
    private queueItem = 0 ;
    private docsQueueItem = 0;
    private repeatDays: string[] = [];
    private customTopics: string[] = [];

    public uploader: MyUploader = new MyUploader({ url: API_URL });
    public docsUploader: MyUploader = new MyUploader({ url: API_URL });
    
    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public eventService: EventService,
        public settingService: SettingsService,
        private router: Router
    ) {

        this.repeatDays.length;
        this.createForm();
        this.initDefaults();

    }
   

    private initDefaults(){
        
        this.customTopics = [];
        this.settingService.findSettings().subscribe(
            data => {
                this.defaultValues = data;
                if (this.defaultValues.freizeit_keywords) {
                    this.freizeit_keywords = this.defaultValues.freizeit_keywords.split(",");
                    this.freizeit_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.workshop_keywords) {
                    this.workshop_keywords = this.defaultValues.workshop_keywords.split(",");
                    this.workshop_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.talk_keywords) {
                    this.talk_keywords = this.defaultValues.talk_keywords.split(",");
                    this.talk_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }

            }
        );

    }

    ngOnInit() {


    }

    public enableConfirm(): void {
        this.confirmShow = true ;
        $('#copyright_confirm_event').prop('disabled', false);
        $('#copyright_confirm_event').prop('checked', false);

    }

    public disableConfirm(): void {
        $('#copyright_confirm_event').prop('disabled', true);

    }

    changeEventType(){

        this.customTopics = [];

    }

    uploadEventPicTemp() {
        // Set uploader description
        this.uploader.onBuildItemForm = (item, form) => {
            form.append("description", 'temp');
        };
        this.uploader.queue.forEach(item => {
            if (item.file.size > 3000000){
                Materialize.toast('Das Bild ist zu groß', 4000, 'red');
                this.uploader.clearQueue();
            }                
            else{
                item.upload(); 
                this.uploader.onSuccessItem = (item:any, response:any, status:any, headers:any) => {
                    if (response == 'NO'){
                        Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus', 4000, 'red');
                        this.uploader.clearQueue();

                    }
                }
                this.queueItem = this.uploader.getIndexOfItem(item);
            }
        });

    }

    uploadEventPic() {
        this.uploader.queue[this.queueItem].upload();
        Materialize.toast('Das Eventbild ist hochgeladen!', 4000, 'green');
        $('#copyright_confirm_event').prop('disabled', true);
    }

    uploadEventDocs() {
        this.docsUploader.uploadAll();
    }

    prevPage() {
        this.switchPage(false);
    }

    nextPage() {
        if(this.viewstep == 2){

            if($('#copyright_confirm_event').prop('checked') != true){
                    Materialize.toast('Bitte Bestätigen Sie dass Sie alle Rechte vom hochgeladenen Bild Haben', 4000, 'red');
                    return;
            }
        }
        this.switchPage(true);
    }

    switchPage(forward: boolean) {

        var formBlockvalid: boolean = false;
        if(forward){
            switch (this.viewstep) {
                case 1:
                    if (this.eventForm.controls['type'].valid &&
                        this.eventForm.controls['min_attendees'].valid &&
                        this.eventForm.controls['max_attendees'].valid
                    ) {
                        formBlockvalid = true;
                    }
                    break;
                case 2:
                    if (this.eventForm.controls['title'].valid
                    ) {
                        formBlockvalid = true;
                    }
                    break;
                case 3:
                        formBlockvalid = true;
                    break;
            }

            if (formBlockvalid) {
                    this.viewstep++;

            }
            else {
            console.log(this.eventForm.errors);
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
            }
        }
        else {
            this.viewstep--;
        }
        
       
    }

    clearForm() {

        this.dataLoaded = true; // form reset only possible until angular RC5
        this.viewstep = 1;
        (<FormControl>this.eventForm.find('title')).updateValue('');
        (<FormControl>this.eventForm.find('description')).updateValue('');
        this.uploader.clearQueue();
        this.docsUploader.clearQueue();
    }

    createForm() {

        this.eventForm = this.formBuilder.group({
            type: ['talk', Validators.required],
            min_attendees: ['6', Validators.required],
            max_attendees: ['12', Validators.required],
            title: ['', Validators.required],
            status: ['prosposal', Validators.required],
            description: ['', Validators.required],
            topic: ['', Validators.nullValidator],
       
        });

    }

    addDay(e) {

        this.repeatDays.push( this.eventForm.find('custom_days').value );
        this.repeatDays = this.repeatDays.filter(function(x){
            return (x !== (undefined || '' || typeof(undefined) ));
        });
    }

    toogleTopic(e) {
        
        let value:string = e.target.id;

        if (e.target.checked){
            // value = value.replace('tag_','');
            this.customTopics.push( value );
        } else {
              this.customTopics.splice( this.customTopics.indexOf(value) );
        }
   
    }

    removeDay(index){
        
        this.repeatDays.splice(index);
    }

    onSubmit(): void {

        if (this.eventForm.valid) {
            
            Materialize.toast('Dein Event wird erstellt...', 4000);

            let multipartform = new FormData();

            (<FormControl>this.eventForm.find('topic')).updateValue(this.customTopics.join(','));
            this.eventService.insertOne(this.eventForm.value, multipartform).then(
                result => {
                    this.dataLoaded = false;
                    if($('#eventAvatar').val() != "" ){
                        this.uploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventPhoto');
                        };
                        this.uploadEventPic();
                    }
                    if($('#eventDocuments').val() !="" ){
                        this.docsUploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventDocument');
                        };
                        this.uploadEventDocs();
                        this.docsQueueItem = 0;
                    }
                    jQuery('#modal1').openModal();
                    this.initDefaults();
                    Materialize.toast('Dein Event wurde erstellt', 4000, 'green');

                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red')
                    return false;
                }
            );


        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }

    }

}
