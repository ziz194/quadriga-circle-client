import { Component, Input, OnInit, NgZone } from '@angular/core';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile, IQCComment, IQCCommentItems, IQCNewComment } from '../shared/interfaces'
import { SETEVENT, DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM, SET_CURRENT_USER_PROFILE } from '../shared/state.actions'
import { EventService } from '../shared/event.service';
import { IMAGE_URL } from '../config';
import { Title } from '@angular/platform-browser';
import { DateFormatPipe } from '../../node_modules/angular2-moment/DateFormatPipe';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventAttandeeIdSearch, EventIsInList} from '../shared/pipe.filters'
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { CustomValidators } from '../shared/validators';
import { ProfileService } from '../shared/profile.service';
import { ContactService } from '../shared/contact.service';
import { SITE_TITLE } from '../config';
import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { MyUploader} from "../profile/myuploader";
import { CommentService } from '../shared/comment.service';
import { SERVER_URL} from '../config';
import { SERVER_PUBLIC_URL} from '../config';
import { DOCS_URL} from '../config';
import { AuthGuardService } from '../shared/auth.guard.service';


import * as moment from 'moment';
import 'moment/locale/de';

const API_URL = SERVER_URL + 'uploadFile';
@Component({
    selector: 'div',
    template: require('./eventdetail.html'),
    inputs: ['selectedQCEvent'],
    directives: [FILE_UPLOAD_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    pipes: [EventIsInList, FilterPipe, CapitalizePipe, DateFormatPipe, SplitJoin, SplitWrapJoin, EventAttandeeIdSearch],
    providers: [EventIsInList, CustomValidators, ProfileService, ContactService, CommentService, EventAttandeeIdSearch, AuthGuardService]
})

export class EventDetail {

    private mailForm: FormGroup;
    private commentForm: FormGroup;
    public qceventitems: Observable<IQCEventItems>;
    public event: Observable<IQCEvent>;
    public qcuserprofile: Observable<IQCUserProfile>;
    private profile: IQCUserProfile;
    public qccommentitems: Observable<IQCCommentItems>;
    public uploader: MyUploader = new MyUploader({ url: API_URL });
    public docsUploader: MyUploader = new MyUploader({ url: API_URL });
    public events_error: Boolean = false;
    public notFound: Boolean = false;
    private isAttended: Boolean = false;
    public IMAGE_URL = IMAGE_URL;
    public SERVER_PUBLIC_URL = SERVER_PUBLIC_URL;
    private SERVER_URL = SERVER_URL;
    public currentevent: IQCEvent;
    public currentuser;
    public currentUserIsOwner: boolean = false;
    public currentevents: IQCEventItems;
    private id: number;
    private cancel_user_id: number;
    public showComment: Boolean = true;
    private DOCS_URL = DOCS_URL;
    public eventPassed: boolean = false ; 


    constructor(
        public eventService: EventService,
        public store: Store<AppState>,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private zone: NgZone,
        private titleService: Title,
        private customValidators: CustomValidators,
        private formBuilder: FormBuilder,
        public profileService: ProfileService,
        private contactService: ContactService,
        public commentService: CommentService,
        private eventAttandeeIdSearch: EventAttandeeIdSearch,
        private qcEventIsInList: EventIsInList
    ) {
        window.scrollTo(0,0);
        moment.locale('de');
        this.event = this.store.select<IQCEvent>('event');
        this.qceventitems = store.select<IQCEventItems>('qceventitems');
        this.qcuserprofile = store.select<IQCUserProfile>('qcuserprofile');
        this.qccommentitems = store.select<IQCCommentItems>('qccommentitems');
        this.isAttended = false;
        this.mailForm = formBuilder.group({
            subject: ['', Validators.required],
            message: ['', Validators.required]
        });

        this.commentForm = formBuilder.group({
            content: ['', Validators.required]
        });

        this.qcuserprofile.subscribe( data => 
        
            this.profileService.findOne(data.id).subscribe(
                data => {
                                this.profile = data;
                                this.cancel_user_id = this.profile.id ;
                                console.log('USER ', data);
                                this.currentevent.isBookmarked = this.qcEventIsInList.transform(this.currentevent, this.profile.bookmarks.split(','));
                            }
            //this.isAttended = this.eventAttandeeIdSearch.transform(this.currentevent.attendees, data.id);

        ));

        this.route.params.subscribe(params => {
            this.id = +params['id'];
            this.eventService.findOne(this.id)
                .subscribe(
                data => {
                    this.currentevent = data;
                    this.store.dispatch({ type: SETEVENT, payload: data })
                    this.titleService.setTitle(this.currentevent.title);
                    this.qcuserprofile.subscribe(data => {
                        if (this.currentevent.user.id === data.id) {
                            this.currentUserIsOwner = true;
                        }

                        //Check if the event is already passed
                        let eventEndDate = new Date(this.currentevent.endtime); 
                        if(eventEndDate <  new Date()){
                            this.eventPassed = true ; 
                        }
                      

                        this.profileService.findOne(data.id).subscribe(
                            data => {
                                this.profile = data;
                                this.currentevent.isBookmarked = this.qcEventIsInList.transform(this.currentevent, this.profile.bookmarks.split(','));
                            }
                        );
                        this.isAttended = this.eventAttandeeIdSearch.transform(this.currentevent.attendees, data.id);
                    
                });

                },

                err => {
                    this.notFound = true;
                    Materialize.toast('Das Event konnte nicht gefunden werden', 4000, 'red');
                }
                )
        });

    }

    addToWaitingList() {

        this.attendEvent();

    }

    attendEvent() {

        if (this.isAttended === true) {

        }
        this.eventService.attend(this.id).then(
            data => {
                let result = data.json();
                Materialize.toast(result.message, 4000, 'green');
                this.isAttended = true;
                this.eventService.findOne(this.id).subscribe(
                    data => {
                        this.currentevent = data;
                        this.store.dispatch({ type: SETEVENT, payload: data })
                    }
                );
            }).catch(
            error => {
                let data = error.json();
                Materialize.toast(data.error.message, 4000, 'red');
            }
            )

    }

    showcancelAttendanceEvent(event: Event, userid: number){
        jQuery('#modalCancelAttendanceEvent').openModal();
        this.cancel_user_id = userid;
        console.log (this.cancel_user_id)
    }

    cancelAttendanceEvent(event: Event) {
        
      
        jQuery('#modalCancelAttendanceEvent').closeModal();
        this.eventService.cancelAttendance(this.id,this.cancel_user_id).then(

            data => {

                let result = data.json();
                this.isAttended = false;
                  
                Materialize.toast(result.message, 4000, 'green');
                document.getElementById('attendee' +this.cancel_user_id.toString()).remove();

                this.eventService.findOne(this.id).subscribe(
                    data => {
                        this.currentevent = data;
                        this.store.dispatch({ type: SETEVENT, payload: data })
                    }
                );
                            }

        ).catch(

            error => {
                console.log('error', error);
                //let data = error.json();// data.error.message
                Materialize.toast('error', 4000, 'red');
            }

            );

    
    

    


    }

    toggleBookmark() {


        if (this.currentevent.isBookmarked) {
            this.profileService.delBookmark(this.profile.id, this.currentevent.id);
        } else {
            this.profileService.addBookmark(this.profile.id, this.currentevent.id);
        }

        this.profileService.findOne(this.profile.id).subscribe(
            data => {
                this.profile = data;
            },
            err => {

            }
        );

        this.currentevent.isBookmarked = !this.currentevent.isBookmarked;
        this.store.dispatch({ type: UPDATE_EVENTITEM, payload: event });
        this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: this.profile });

    }

    sendMail() {

        let payLoad: Object = this.mailForm.value;
        payLoad['event'] = this.id;

        if (this.mailForm.valid) {
            Materialize.toast('Deine Mail wird gesendet...', 4000);
            this.contactService.sendMailToAttendees(payLoad).then(
                result => {
                    jQuery('#modal1').closeModal();
                    Materialize.toast('Deine Mail wurde an alle Teilnehmer gesendet', 4000, 'green');
                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgeteten.', 4000, 'red')
                    return false;
                }
                );

        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }

    }

    showDeleteModal() {
        jQuery('#modalDeleteEvent').openModal();
    }

    deleteEvent(confirm: boolean) {
        if (confirm) {
            this.eventService.delete(this.id).then(
                result => {
                    jQuery('#modalDeleteEvent').closeModal();
                    Materialize.toast('Dein Event wurde gelöscht', 4000, 'green');
                    this.router.navigate(['/events']);
                }
            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.', 4000, 'red')
                    return false;
                }
                );
        }
        else jQuery('#modalDeleteEvent').closeModal();

    }

    saveComment() {

        if (this.commentForm.valid) {
            this.showComment = false;
            Materialize.toast('Deine Kommentar wird gespeichert...', 4000);
            let payLoad: IQCNewComment = this.commentForm.value;
            payLoad.event = this.id;
            this.commentService.insertOne(payLoad).then(
                result => {
                    this.commentForm.controls['content'].setErrors(null);
                    (<FormControl>this.commentForm.find('content')).updateValue('');
                    Materialize.toast('Deine Kommentar wurde gespeichert', 4000, 'green');
                    this.eventService.sendCommentMail(this.id);
                    this.eventService.findOne(this.id).subscribe(
                        data => {
                            this.currentevent = data;
                            this.store.dispatch({ type: SETEVENT, payload: data })
                        }
                    );
                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgeteten.', 4000, 'red')
                    return false;
                }
                );

        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }

    }
    // Change to default pic if the source is broken 
    updateUrlAvatar(event) {
        var target = event.target || event.srcElement || event.currentTarget;
        $(target).attr('src', SERVER_PUBLIC_URL + 'img/default/missing.png')
    }


}
