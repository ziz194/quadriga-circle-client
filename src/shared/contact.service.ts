import { Injectable } from '@angular/core';
import { SERVER_URL } from '../config';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import {AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let favorites = [],
    propertiesURL = SERVER_URL + 'contact';

@Injectable()
export class ContactService {

    private result: string[];

    constructor(private http: Http, private authHttp: AuthHttp) {

    }

    private extractData(res: Response) {

        let body = res.json();
        return body.data || {};
    }

    

    sendRequest(formData): Promise<any> {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        let options = new RequestOptions({ headers: headers });
        options.withCredentials = true;
        if (!formData.captcha) {
            return this.authHttp.post(propertiesURL, JSON.stringify(formData), options).toPromise();
        } else {
            return this.http.post(propertiesURL, JSON.stringify(formData), options).toPromise();
        }
    }

    sendMailToAttendees(formData): Promise<any> {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        let options = new RequestOptions({ headers: headers });

        return this.http.post(propertiesURL + "/attendees", JSON.stringify(formData), options).toPromise().then(respsonse => respsonse.json());


    }

}