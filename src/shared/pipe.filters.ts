import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(value: any, args: string[]): any {
        let filter = args[0];

        if (filter && Array.isArray(value)) {
            let filterKeys = Object.keys(filter);
            return value.filter(item =>
                filterKeys.reduce((memo, keyName) =>
                    memo && item[keyName] === filter[keyName], true));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

    transform(value: String) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
    }
}


@Pipe({
    name: 'qcsplitjoin'
})
export class SplitJoin implements PipeTransform {

    transform(value: String, split: string = ',', join: string = ', ') {
        if (value) {
            return value.split(split).join(join);
        }
    }
}


@Pipe({
    name: 'qcsplitwrapjoin'
})
export class SplitWrapJoin implements PipeTransform {

    transform(value: String, split: String = ',', wrapBefore: string = '<div class="chip">', wrapAfter: string = '</div>', joinThis: string = '') {
        if (value) {
            var newVal = '';
            var arrVal = value.split(',');
            arrVal.forEach(element => {
                newVal += wrapBefore + element + wrapAfter + joinThis;
            });
            return newVal;

        }
    }
}

@Pipe({
    name: 'qceventsearch'
})
export class EventSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            console.log(search);
            return value.filter((item) => (
                item.description.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.title.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.host_city.toLowerCase().indexOf(search.toLowerCase()) != -1
            ));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qceventtopicsearch'
})
export class EventTopicSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.topic.toLowerCase().indexOf(search.toLowerCase()) != -1));
        } else {
            return value;
        }
    }
}


@Pipe({
    name: 'qceventtypesearch'
})
export class EventTypeSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            var typeSearch: string;
            typeSearch = search.toLowerCase();
            switch (typeSearch) {
                case "freizeit":
                return value.filter((item) => (item.type == 'freizeit'));
                case "talk":
                return value.filter((item) => (item.type == 'talk'));
                case "workshops":
                return value.filter((item) => (item.type == 'workshop'));
            }
        } else {
            return value;
        }
    }
}


@Pipe({
    name: 'qceventuseridsearch'
})
export class EventUserIdSearch implements PipeTransform {

    transform(value, search: number = 0) {
        if (search != 0) {
            return value.filter((item) => (item.user.id == search));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qceventattendeeidsearch'
})
export class EventAttandeeIdSearch implements PipeTransform {


    transform(value, search: number = 0) {
        let found: boolean = false;

        if (value != null && search != 0) {
            for (var e = 0; e < value.length; e++) {
                var element = value[e];
                if (element.id === search) {
                    found = true;
                }
            }
            return found;
        } else {
            return found;
        }
    }
}

@Pipe({
    name: 'qcEventIsInList'
})
export class EventIsInList implements PipeTransform {

    transform(value, inIdList: Array<any>): any {

            for (var e = 0; e < inIdList.length; e++) {
                console.log('xxx', inIdList[e], value.id, value.id == inIdList[e]);
                if (value.id == inIdList[e]) {

                    return true;
                }
            }
       
    }
}

@Pipe({
    name: 'qcProfileTopicSearch'
})
export class ProfileTopicSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.occupational_area.toLowerCase().indexOf(search.toLowerCase()) != -1)
            );
        } else {
            return value;
        }
    }
}


@Pipe({
    name: 'qcprofilesearch'
})
export class ProfileSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            console.log(search);
            return value.filter((item) => (
                item.firstname.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.lastname.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.occupational_area.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.career_level.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.suffix.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.company.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.title.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.about.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.company_street.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.company_zip.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.company_city.toLowerCase().indexOf(search.toLowerCase()) != -1
            ));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'arrayify'
})
export class ArrayifyPipe {
    transform(val) {
        return Array.isArray(val)
            ? val : [val];
    }
}