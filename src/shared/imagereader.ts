
export class base64Eencode {
 
    private fileReader: FileReader;
    private base64Encoded: string;
 
    constructor() {
        this.fileReader.onload = (file) => {
            this.base64Encoded = this.fileReader.result;
            console.log("Encoded file!");
        }
    }
 
    encodeFile(file : File) {
        this.fileReader.readAsDataURL(file);
    }
}