import {
  expect, it, iit, xit,
  describe, ddescribe, xdescribe,
  beforeEach, beforeEachProviders, withProviders,
  async, inject, fakeAsync, 
} from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Headers, RequestOptions, Http, Response, HTTP_PROVIDERS} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { Router, provideRouter } from '@angular/router';
import { provide } from '@angular/core';
import { AuthService } from './auth.service.ts'; 

describe('Service: AuthService', () => {
  let service;
  
  //setup
  beforeEachProviders(() => [
    AuthHttp, 
    Http, 
    HTTP_PROVIDERS, 
    AuthService, 
    provide(AuthConfig, { useValue: new AuthConfig() })
  ]);
  
  beforeEach(inject([AuthService], s => {
    service = s;
  }));
  
  //specs
  it('should return available languages', () => {
    let languages = service.get();
    expect(languages.length).toEqual(3);
  });

  it('reset new password email', () => {
    let languages = service.sendResetMail();
    console.log(languages);
    expect(languages.length).toEqual(3);
  });

}) 