import {Injectable} from '@angular/core';
import {SERVER_URL} from '../config';
import {Headers, RequestOptions, Http, Response} from '@angular/http';
import {AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import {Observable} from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile, IQCEvent } from '../shared/interfaces'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let favorites = [],
    propertiesURL = SERVER_URL + 'events',
    favoritesURL = propertiesURL + 'events';

@Injectable()
export class EventService {

    public events;
    public event;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState>) {
        this.events = [];
    }

    ngOnInit() {
        this.findAll();
    }

    //just future events
    findAll() {

        this.events = this.authHttp.get(propertiesURL).map(this.extractData).catch(this.handleError);
        return this.events;

    }

    findAllInTimePeriod(timePeriod) {

        return this.authHttp.get(propertiesURL+'?timePeriod='+timePeriod).map(this.extractData).catch(this.handleError);

    }
     findPrososal() {

        this.events = this.authHttp.get(propertiesURL +  '?status=prosposal').map(this.extractData).catch(this.handleError);
        return this.events;

    }


    findAllByDate(date1, date2) {

        this.events = this.authHttp.get(propertiesURL+'/eventsByDate/' +date1+'/'+date2 ).map(this.extractData).catch(this.handleError);
        return this.events;

    }

    findOne(id: number) {

        this.event = this.http.get(propertiesURL + '/' + id).map(this.extractData).catch(this.handleError);
        return this.event;

    }

    updateOne(id: number, event: IQCEvent, formData) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.put(propertiesURL + '/' + id, JSON.stringify(event), options).toPromise();

    }

    insertOne(event: IQCEvent, formData) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL, JSON.stringify(event), options).map(this.extractData).toPromise();

    }

    delete(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.delete(propertiesURL + '/' + id, options).toPromise();

    }


    private extractData(res: Response) {

        let body = res.json();
        return body.data || {};
    }

    attend(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
       // console.log(localStorage.getItem('auth_token'));
        return this.authHttp.post(propertiesURL + '/' + id + '/attend', '', { headers }).toPromise();

    }

    cancelAttendance(id: number, userid: number){

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.delete(propertiesURL + '/' + id + '/attend/' + userid , { headers } ).toPromise();
    }

    sendCommentMail(id: number){

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.post(propertiesURL + '/sendCommentMail' , JSON.stringify({ event: id}), { headers }).toPromise();
  

    }

    sendMailToAttendees(id: number, subject: string, message: string) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.post(propertiesURL + '/' + id + '/message', JSON.stringify({ subject: subject, message: message, event: id }), { headers }).toPromise();

    }


    public addUsertoWaitinglist(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.post(propertiesURL + '/' + id + '/wait', '', { headers }).toPromise();


    }

    handleError(error) {
     
        return Observable.throw(error.json().error || 'Server error');
    
}

}