import {Injectable} from '@angular/core';
import {SERVER_URL} from '../config';
import {Headers, RequestOptions, Http, Response} from '@angular/http';
import {AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import {Observable} from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQCFaqItems, IQCFaq } from '../shared/interfaces'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let propertiesURL = SERVER_URL + 'faqs';

@Injectable()
export class FAQService {

    public faqs;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState> ) {
        this.faqs = [];
    }

    ngOnInit() {
        this.findAll();
    }

    findAll() {
        return this.authHttp.get(propertiesURL).toPromise();
    }

    private extractData(res: Response) {

        let body = res.json();
        return body.data || {};
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}