import { Control } from '@angular/common';
import { Injectable} from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class CustomValidators {

  
    static validateEmail(c: FormControl) {

        return (c.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) || c.value === '') ? null : {
            validateEmail: {
                valid: false
            }
        };
    }

    static validateXingUrl(c: FormControl) {

        let START_WITH = 'https://www.xing.com';

        return (c.value.startsWith(START_WITH) || c.value === '') ? null : {
            validateXingUrl: {
                valid: false
            }
        };
    }

    static validateLinkedInUrl(c: FormControl) {

        let START_WITH = 'https://www.linkedin.com';

        return (c.value.startsWith(START_WITH) || c.value === '') ? null : {
            validateLinkedInUrl: {
                valid: false
            }
        };
    }
    
    static validateFacebookUrl(c: FormControl) {

        let START_WITH = 'https://www.facebook.com';

        return (c.value.startsWith(START_WITH) || c.value === '') ? null : {
            validateFacebookUrl: {
                valid: false
            }
        };
    }

}

