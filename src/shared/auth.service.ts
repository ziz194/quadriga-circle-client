import { Injectable} from '@angular/core';
import { SERVER_URL} from '../config';
import { Headers, RequestOptions, Http, Response} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, JwtHelper} from 'angular2-jwt';
import { Observable} from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile, IQCEvent } from '../shared/interfaces'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let propertiesURL = SERVER_URL + 'auth'

@Injectable()
export class AuthService {

    private jwtHelper: JwtHelper = new JwtHelper();
    private token = "" ; 

    public profiles;
    public profile;

    constructor(private http: Http, public authHttp: AuthHttp, public store: Store<AppState>) {
    }

    useJwtHelper() {
        this.token  = localStorage.getItem('auth_token');
        return " //Token Expiration Date :  " + this.jwtHelper.getTokenExpirationDate(this.token) + " //Is Token Expired : " + this.jwtHelper.isTokenExpired(this.token);
    }
    isExpired() {
        this.token = localStorage.getItem('auth_token');
        return this.jwtHelper.isTokenExpired(this.token);
    }

    authenticate(email, password, token: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', token);

        return this.authHttp.post(SERVER_URL + 'auth', JSON.stringify({ email: email, password: password }), {
            headers: headers
        }

        ).toPromise();
    }

    user() {

        return this.authHttp.get(SERVER_URL + 'auth/user').toPromise();

    }

    sendResetMail(mail) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL + '/forgot-password', JSON.stringify({ email: mail }), options).toPromise();

    }

    sendNewPassword(password, token) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL + '/reset-password', JSON.stringify({ token: token, password: password, password_confirmation: password }), options).toPromise();

    }

    private extractData(res: Response) {
        let body = res.json();
        return body.user || body.data;
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}