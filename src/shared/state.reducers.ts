import { ActionReducer, Action, Reducer, combineReducers } from '@ngrx/store';
import { IQCEvent, IQCEventItems, IQCUserProfileItems, IQCUserProfile, IQCBookmarks } from '../shared/interfaces';

import {
    ADD_EVENT,
    INCREMENT,
    DECREMENT,
    RESET,
    SETEVENT,
    SETVAL,
    SETEVENTITEMS,
    SETPASTEVENTITEMS,
    SETALLEVENTITEMS,
    SET_CURRENT_USER_PROFILE,
    ADD_EVENT_BOOKMARK,
    DEL_EVENT_BOOKMARK,
    UPDATE_EVENTITEM,
    SET_PROFILE_ITEMS,
    SETUSERPROFILEITEMS,
    SETPROSPOSAL,
    UPDATE_PROSPOSAL
} from './state.actions';


export const counterReducer: ActionReducer<number> = (state: number = 1, action: Action) => {
    switch (action.type) {
        case INCREMENT:
            return state + 1;

        case DECREMENT:
            return state - 1;

        case RESET:
            return 0;

        case SETVAL:
            return action.payload;

        default:
            return state;
    }
}

export const detailsReducer = (state: string = 'ddd', action: Action) => {
    switch (action.type) {
        case ADD_EVENT:
            return state;
        default:
            return state;
    }
}

export const eventReducer: ActionReducer<IQCEvent> = (state: IQCEvent, action: Action) => {

    switch (action.type) {
        case SETEVENT:
            console.log(action.payload);
            state = action.payload;
            return state;

        default:
            return state;
    }
}

export const qceventitemsReducer: ActionReducer<IQCEventItems> = (state: IQCEventItems, action: Action) => {

    switch (action.type) {
        case SETEVENTITEMS:
            state = action.payload;
            return state;
        case UPDATE_EVENTITEM:
            return state.map(event => {
                if ( event.id === action.payload.id) {
                    event = action.payload;
                    return event;
                } else {
                    return event;
                }
            });
        default:
            return state;

    }
}


export const qcuserprofileitemsReducer: ActionReducer<IQCUserProfileItems> = (state: IQCUserProfileItems, action: Action) => {

    switch (action.type) {
        case SETUSERPROFILEITEMS:
            state = action.payload;
            return state;
        case UPDATE_EVENTITEM:
            return state.map(event => {
                if ( event.id === action.payload.id) {
                    event = action.payload;
                    return event;
                } else {
                    return event;
                }
            });
        default:
            return state;

    }
}



export const qceventprosposalsReducer: ActionReducer<IQCEventItems> = (state: IQCEventItems, action: Action) => {

    switch (action.type) {
        case SETPROSPOSAL:
            state = action.payload;
            return state;
        case UPDATE_PROSPOSAL:
            return state.map(event => {
                if ( event.id === action.payload.id) {
                    event = action.payload;
                    return event;
                } else {
                    return event;
                }
            });
        default:
            return state;

    }
}

export const qcuserprofileReducer: ActionReducer<IQCUserProfile> = (state: IQCUserProfile, action: Action) => {

    switch (action.type) {
        case SET_PROFILE_ITEMS:
            state = action.payload;
            return state;
        case SET_CURRENT_USER_PROFILE:
            state = action.payload;
            return state;
        default:
            return state;


    }
}

export const qcEventBookmarksReducer: ActionReducer<IQCBookmarks> = (state: IQCBookmarks = [], action: Action) => {

    switch (action.type) {
        case ADD_EVENT_BOOKMARK:
            state.push(action.payload);
            return state;
        case DEL_EVENT_BOOKMARK:
            for (var i = 0, len = state.length; i < len; i++) {
                if (state[i] === action.payload) {
                    state.splice(i, 1);
                }
            }
            return state;
        default:
            return state;

    }
}