import { Injectable } from '@angular/core';
import { SERVER_URL } from '../config';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile, IQCComment, IQCNewComment } from '../shared/interfaces'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let URL = SERVER_URL + 'http://192.168.10.10/renderImage';

@Injectable()
export class ImageService {

    public image;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState> ) {
        this.image = '';
    }



    renderImage(width:string,image: string) {
        this.authHttp.get(URL + '/' + width+ '/' + image).subscribe(
                data => this.image = data.text(),
                err => console.log(err.text())
        );
        return this.image ;
    }

    private extractData(res: Response) {

        let body = res.json();
        return body.user || body.data;
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}