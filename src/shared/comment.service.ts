import { Injectable } from '@angular/core';
import { SERVER_URL } from '../config';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile, IQCComment, IQCNewComment } from '../shared/interfaces'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let propertiesURL = SERVER_URL + 'comments';

@Injectable()
export class CommentService {

    public comments;
    public comment;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState> ) {
        this.comments = [];
    }

    ngOnInit() {
        this.findAll();
    }

    findAll() {
        this.comments = this.authHttp.get(propertiesURL).map(this.extractData).catch(this.handleError);
        return this.comments;
    }

    insertOne(comment: IQCNewComment) {
        this.comments.push(comment);
        return this.authHttp.post(propertiesURL + '/' + SERVER_URL + 'events/' + comment.event , JSON.stringify(comment)).toPromise();
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}