import { Component } from '@angular/core';
import { SERVER_URL } from '../config';
import { SITE_TITLE } from '../config';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, provideRouter } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location, NgClass } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState, IQCFaqItems, IQCFaq  } from '../shared/interfaces';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { Title } from '@angular/platform-browser';
import { DOCS_URL } from '../config'



@Component({
    selector: 'div',
    template: require('./info.html'),
    providers: [],
    directives: [ROUTER_DIRECTIVES, Sidebar, Footer, MaterializeDirective]
})

export class Info {

        private DOCS_URL = DOCS_URL;

    constructor(public store: Store<AppState>,
        private titleService: Title
    ) {
        this.titleService.setTitle(SITE_TITLE); 

    }

}