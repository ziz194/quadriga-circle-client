import { Component } from '@angular/core';
import { SITE_TITLE } from '../config';

import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { HTTP_PROVIDERS, Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { SERVER_URL} from '../config';
import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localstorage/LocalStorageEmitter';
import { Observable}  from 'rxjs/Rx';
import { ProfileService } from '../shared/profile.service';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile } from '../shared/interfaces'
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { INCREMENT, DECREMENT, RESET, SETVAL, SETEVENTITEMS, SETPASTEVENTITEMS, SETALLEVENTITEMS, SET_PROFILE_ITEMS, SETUSERPROFILEITEMS } from '../shared/state.actions';




@Component({
    selector: 'div',
    template: require('./login.html'),
    directives: [REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header],
    providers: [ProfileService]

})

export class LoginForm {

    private qcuserprofile: Observable<IQCUserProfile>;
    private isSent: boolean = false;
    private isError: boolean = false;
    private loginForm: FormGroup;

    constructor(

        private formBuilder: FormBuilder,
        public profileService: ProfileService,
        private router: Router,
        private http: Http,
        public authHttp: AuthHttp,
        public store: Store<AppState>

    ) {

        this.loginForm = formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

    }

    ngOnInit(): void {

        if (localStorage.getItem('user')) {
            this.router.navigate(['/events']);
        }

    }

    onSubmit() {

        var headers = new Headers();
        var body = '{"email":"' + this.loginForm.controls['email'].value + '","password":"' + this.loginForm.controls['password'].value + '"}';

        if (localStorage.getItem('auth_token') !== 'x') {

            headers.append('Content-Type', 'text/json');
            headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));

            this.http.post(SERVER_URL + 'auth', body, {
                headers: headers
            }
            ).subscribe(
                data => {

                    localStorage.setItem('auth_token', data.json().token);
                    this.authHttp.get(SERVER_URL + 'auth/user', headers)
                        .subscribe(
                        data => {

                            this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data.json().user });
                            localStorage.setItem('user', data.json().user);
                            this.router.navigate(['/events']);

                        },
                        err => console.log(err)
                        );

                },
                err => Materialize.toast('Dein Benutzername/Passwort ist falsch', 4000, 'red')
                );

        }

    }

}