import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { HTTP_PROVIDERS, Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { SERVER_URL} from '../config';
import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localstorage/LocalStorageEmitter';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile } from '../shared/interfaces'
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { AuthService } from '../shared/auth.service'

@Component({
    selector: 'div',
    template: require('./password.html'),
    directives: [REACTIVE_FORM_DIRECTIVES, Sidebar, Footer, Header]
})

export class PasswordForm {

    private qcuserprofile: Observable<IQCUserProfile>;
    private isSent: boolean = false;
    private isError: boolean = false;
    private passwordForm: FormGroup;
    private passwordResetForm: FormGroup;
    private token: string;

    constructor(

        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private http: Http,
        public authHttp: AuthHttp,
        public store: Store<AppState>,
        private authService: AuthService

    ) {

        this.passwordForm = formBuilder.group({
            email: ['', Validators.required]
        });
        this.passwordResetForm = formBuilder.group({
            password: ['', Validators.required],
            password_repeat: ['', Validators.required]
        });
        console.log(this.token);
    }

    ngOnInit(): void {

        this.route.params.subscribe(
            params => {
                this.token = params['token'];
                console.log(this.token);
            }
        );

        if (localStorage.getItem('user')) {
            this.router.navigate(['/events']);
        }

    }

    onSubmit() {

        this.authService.sendResetMail(this.passwordForm.controls['email'].value).then(
            response => {
                this.isSent = true;
            }
        ).catch(
            error => {
                let data = error.json();
                console.log(data.error.message);
                if (data.error.message == "Email is not valid") {
                    Materialize.toast('Dies ist keine gültige Email.', 4000, 'red')
                } 
                else if (data.error.message == "User not found") {
                    Materialize.toast('Der Nutzer existiert nicht', 4000, 'red')
                }else {
                    this.isSent = true;
                }

            }
            );

    }


    onSubmitPassword() {
        if (this.passwordResetForm.controls['password'].value != this.passwordResetForm.controls['password_repeat'].value){
                 Materialize.toast('Passwörter stimmen nicht überein.', 4000, 'red')
        } else {
            this.authService.sendNewPassword(this.passwordResetForm.controls['password'].value, this.token).then(
                response => {
                    Materialize.toast('dein Passwort wurde gespeichert und du kannst dich jetzt damit einloggen', 4000, 'green')
                    this.router.navigate(['/login']);
                }
            ).catch(
                error => {
                    let data = error.json();
                    Materialize.toast(data.error.message, 4000, 'red')
                }
                );

        }
    }

}