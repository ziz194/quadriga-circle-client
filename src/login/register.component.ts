import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { HTTP_PROVIDERS, Http, Headers} from '@angular/http';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { SERVER_URL} from '../config';
import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localstorage/LocalStorageEmitter';
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile } from '../shared/interfaces';
import { MaterializeDirective } from "angular2-materialize";
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { ProfileService } from '../shared/profile.service';
import { AuthService } from '../shared/auth.service';


@Component({
    selector: 'div',
    template: require('./register.html'),
    directives: [REACTIVE_FORM_DIRECTIVES, Sidebar, Footer,ROUTER_DIRECTIVES, Header,MaterializeDirective]
})

export class RegisterForm {

    private qcuserprofile: Observable<IQCUserProfile>;
    private isSent: boolean = false;
    private isError: boolean = false;
    private registerForm: FormGroup;

    constructor(

        private formBuilder: FormBuilder,
        private router: Router,
        private http: Http,
        public authHttp: AuthHttp,
        public store: Store<AppState>,
        private profileService: ProfileService,
        private authService: AuthService

    ) {

        this.registerForm = formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

    }

    ngOnInit(): void {

        if (localStorage.getItem('user')) {
            this.router.navigate(['/events']);
        }

    }

    onSubmit() {

    

        Materialize.toast('Dein Konto wird gespeichert...', 4000);
        this.profileService.create(this.registerForm.controls['email'].value, this.registerForm.controls['password'].value).then(
            response => {
                let data = response.json();

                var headers = new Headers();
                var body = '{"email":"' + this.registerForm.controls['email'].value + '","password":"' + this.registerForm.controls['password'].value + '"}';

        if (localStorage.getItem('auth_token') !== 'x') {
                headers.append('Content-Type', 'text/json');
                headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));

                
                this.http.post(SERVER_URL + 'auth', body , {
                    headers: headers
                }
                ).subscribe(
                    data => {
                      console.log('auth_token', data.json()) ;
                       localStorage.setItem('auth_token', data.json().token);
                        this.authHttp.get(SERVER_URL + 'auth/user', headers)
                            .subscribe(
                            data => {
                                this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data.json().user });
                                localStorage.setItem('user', data.json().user);
                                let user = data.json().user;
                                // Materialize.toast('Deine Registrieung ist abgeschlossen. Bitte vervollständige dein Profil.', 4000, 'green');
                                jQuery('#modal1').openModal();
                                // this.router.navigate(['/profile/edit']);
                            },
                            err => console.log(err)
                            );

                    },
                    err => console.log('ERROR' + err)
                    );
            }
            }
        ).catch(
            error => {
                let data = error.json();
                Materialize.toast(data.error.message, 4000, 'red')
            }
            );



    }

}