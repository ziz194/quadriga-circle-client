import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { Title } from '@angular/platform-browser';
import { SITE_TITLE } from '../config';



@Component({
  selector: 'div',
  template: require('./imprint.html'),
  directives: [ROUTER_DIRECTIVES, Sidebar, Footer, Header]
})
export class Imprint {

  constructor(
    private titleService: Title
  ) {
    this.titleService.setTitle(SITE_TITLE);
  }

}