
//import 'systemjs';
import 'es6-shim';
import 'reflect-metadata';
import 'core-js';
import 'zone.js/dist/zone';

import { provide, enableProdMode, Component } from '@angular/core';
import { Angulartics2 } from 'angulartics2';
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { provideRouter} from '@angular/router';
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { AppComponent, routes } from './app.component';
import { SERVER_URL } from './config';
import { AuthGuardService } from './shared/auth.guard.service';
import { AuthService } from './shared/auth.service';

import { counterReducer,
    eventReducer,
    detailsReducer,
    qceventitemsReducer,
    qcuserprofileitemsReducer,
    qcuserprofileReducer,
    qcEventBookmarksReducer,
    qceventprosposalsReducer } from './shared/state.reducers';

import { Title } from '@angular/platform-browser';
import { provideStore, combineReducers } from '@ngrx/store';
import { compose } from "@ngrx/core/compose";
import { localStorageSync } from "ngrx-store-localstorage";
import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localstorage/LocalStorageEmitter';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import "materialize-css";
import "angular2-materialize";

require('../assets/sass/styles.scss');

if (app.environment === 'production') {
    enableProdMode();
} else {
    console.info('QC ENV: ', app.environment, SERVER_URL);
}

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
    ];

bootstrap(AppComponent, [
    Title,
    HTTP_PROVIDERS,
    APP_ROUTER_PROVIDERS,
    AuthService,
    AuthGuardService,
    Angulartics2,
    disableDeprecatedForms(),
    provideForms(),
    provide(LocationStrategy, { useClass: PathLocationStrategy }),
    provide(APP_BASE_HREF, { useValue: '/' }),
    provideStore(
        compose(localStorageSync(['qcuserprofile', 'qceventitems', 'qcpasteventitems', 'qcalleventitems','qcuserprofileitems', 'qceventbookmarks', 'qceventprosposals'], true),
            combineReducers
        )({
            details: detailsReducer,
            event: eventReducer,
            counter: counterReducer,
            qceventitems: qceventitemsReducer,
            qcuserprofile: qcuserprofileReducer,
            qceventbookmarks: qcEventBookmarksReducer,
            qcuserprofileitems: qcuserprofileitemsReducer,
            qceventprosposals: qceventprosposalsReducer
        })
    ),
    provide(AuthConfig, {
        useValue: new AuthConfig(
            {
                headerName: 'Authorization',
                headerPrefix: 'Bearer ',
                tokenName: 'token',
                tokenGetter: (() => localStorage.getItem('auth_token')),
                globalHeaders: [{ 'Content-Type': 'application/json' }],
                noJwtError: true,
                noTokenScheme: true
            }
        )
    }),
    AuthHttp
]
).catch(console.error);

jQuery(function () {
    jQuery.extend(jQuery.fn.pickadate.defaults, {
        monthsFull: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthsShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        weekdaysFull: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        weekdaysLetter: ['S', 'M', 'D', 'M', 'D', 'F', 'S'],
        today: '',
        close: 'Schließen',
        firstDay: 1,
        min: true,
        closeOnSelect: true,
        format: 'dd.mm.yyyy',
        formatSubmit: 'yyyy-mm-dd',
        clear: 'Löschen'
    });

});
