import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { Header } from '../partials/header.component';
import { SETEVENT, ADD_EVENT, INCREMENT, DECREMENT, RESET, SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile } from '../shared/interfaces'
import { IMAGE_URL , SERVER_URL } from '../config'
import { MaterializeDirective } from "angular2-materialize";
import { ProfileService } from '../shared/profile.service';



@Component({
    selector: 'sidebar',
    template: require('./sidebar.html'),
    directives: [ROUTER_DIRECTIVES, MaterializeDirective,Header],
    providers: [ProfileService]
})


export class Sidebar implements OnInit {

    counter: Observable<number>;
    event: Observable<IQCEvent>;
    qceventitems: Observable<IQCEventItems>;
    qcuserprofile: Observable<IQCUserProfile>;
    private profile: IQCUserProfile;
    IMAGE_URL = IMAGE_URL;
    SERVER_URL = SERVER_URL;
    parentValue: string = 'init';
    avatar: string ;
    loginLogoutText: string = 'Login';
    isCollapsed: boolean = false;

    constructor(//private authService: AuthService, 
        public store: Store<AppState>,
        private router: Router,
        private profileService: ProfileService,
        private location: Location) {

        this.counter = this.store.select<number>('counter');
        this.event = this.store.select<IQCEvent>('event');
        this.qceventitems = store.select<IQCEventItems>('qceventitems');
        this.qcuserprofile = store.select<IQCUserProfile>('qcuserprofile');


    }

    ngOnInit() {
    }

    loginOrOut() {
    };

    setLoginLogoutText(loggedIn: boolean) {
        this.loginLogoutText = (loggedIn) ? 'Logout' : 'Login';
    }

    redirectToLogin() {
        this.router.navigate(['/login']);
    }

    highlight(path: string) {
        return this.location.path() === path;
    }

    increment() {
        this.store.dispatch({ type: INCREMENT });
    }

    decrement() {
        this.store.dispatch({ type: DECREMENT });
    }

    reset() {
        this.store.dispatch({ type: RESET });
    }

    setValue(int) {
        // console.log('->>>>>setValue store value'+ int)
        this.store.dispatch({ type: ADD_EVENT, payload: '7' });
    }

    


    logout() {

        console.log('logout');
        localStorage.setItem('auth_token', '');
        localStorage.setItem('user', '');
        this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: null });
        return true;
    }
    updateUrl(event) { 
        var target = event.target || event.srcElement || event.currentTarget;
        $(target).attr('src', IMAGE_URL+'missing.png');
    }


}