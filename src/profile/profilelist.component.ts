import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, CanActivate} from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState, IQCUserProfile, IQCUserProfileItems } from '../shared/interfaces'
import { Location } from '@angular/common';
import { IMAGE_URL } from '../config';
import { Title } from '@angular/platform-browser';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { ProfileService } from '../shared/profile.service';
import { DateFormatPipe } from '../../node_modules/angular2-moment/DateFormatPipe';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, ProfileSearch, ProfileTopicSearch, ArrayifyPipe } from '../shared/pipe.filters';
import { PaginatePipe, PaginationControlsCmp, PaginationService } from 'ng2-pagination';
import { INCREMENT, DECREMENT, RESET, SETVAL, SETEVENTITEMS, SETPASTEVENTITEMS, SETUSERPROFILEITEMS, SETALLEVENTITEMS, SET_PROFILE_ITEMS } from '../shared/state.actions';
import { SERVER_URL } from '../config';
import { AuthService } from '../shared/auth.service';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';




@Component({
    selector: 'div',
    template: require('./profilelist.html'),
    directives: [ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective, PaginationControlsCmp],
    providers: [ProfileService, PaginationService, AuthService],
    pipes: [DateFormatPipe, PaginatePipe, FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, ProfileSearch, ProfileTopicSearch, ArrayifyPipe]
})

export class ProfileList {

    public qcprofileitems: Observable<IQCUserProfileItems>;
    public topicSearch = '';
    public topicSearchDefault: string = 'Alle Tätigkeitsschwerpunkte';
    private IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;



    constructor(public store: Store<AppState>,
        public profileService: ProfileService,
        private router: Router,
        private location: Location,
        private auth: AuthService,
        private titleService: Title
    ) { 

    this.titleService.setTitle("Profilliste");     
    this.qcprofileitems = store.select<IQCUserProfileItems>('qcuserprofileitems');   
    this.getProfiles();
    }


    private getProfiles() {

        this.profileService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETUSERPROFILEITEMS, payload: data });;
            },
            err => { console.log(err) }
        );

    }

    filterTopic(search) {
        this.topicSearch = search;
        if (this.topicSearch === '') {
            this.topicSearchDefault = 'Alle Tätigkeitsschwerpunkte';
        } else {
            this.topicSearchDefault = search;
        }
    }

    pageChanged($event) :void{

        window.scrollTo(0, 0);

    }

    // Change to default pic if the source is broken 
    updateUrlAvatar(event) { 
        var target = event.target || event.srcElement || event.currentTarget;
        $(target).attr('src',IMAGE_URL+'missing.png')
    }

    updateUrlEvent(event) { 
        var target = event.target || event.srcElement || event.currentTarget;
        $(target).attr('src',IMAGE_URL+'missing-event.jpg')
    }


}
