import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';

export class MyUploader extends FileUploader {
  onAfterAddingFile(file: any) {
    file.withCredentials = false;
  }
}