import { Component, Directive } from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Title } from '@angular/platform-browser';
import { AppState, IQCEvent, IQCEventItems, IQCUserProfile, IQApplicationDefault } from '../shared/interfaces'
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Store } from '@ngrx/store';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { ProfileService } from '../shared/profile.service';
import { CustomValidators } from '../shared/validators';
import { AuthService } from '../shared/auth.service';
import { IMAGE_URL } from '../config'
import { base64Eencode } from '../shared/imagereader';
import { SERVER_URL} from '../config';
import { FILE_UPLOAD_DIRECTIVES, FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { MyUploader} from "./myuploader";
import { SettingsService } from '../shared/setting.service';
import { AuthGuardService } from '../shared/auth.guard.service';

    const API_URL = SERVER_URL + 'uploadFile';
@Component({
    selector: 'div',
    template: require('./profileedit.html'),
    directives: [FILE_UPLOAD_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    providers: [ProfileService, SettingsService, AuthGuardService]
})


export class ProfileEdit {

    private qcuserprofile: Observable<IQCUserProfile>;
    private profileForm: FormGroup;
    private IMAGE_URL = IMAGE_URL;
    private profile: IQCUserProfile;
    private occupational_areas: string[]; ;
    private career_levels: string[]; 
    private queueItem = 0;
    public confirmShow: boolean = false ; 
    public settingValues:IQApplicationDefault;
    public  uploader:MyUploader = new MyUploader({url: API_URL});

    constructor(
        private formBuilder: FormBuilder,
        private store: Store<AppState>,
        private profileService: ProfileService,
        private titleService: Title,
        private router: Router,
        private settingService: SettingsService,
        private authService: AuthService

    ) {

        this.settingService.findSettings().subscribe(
            data => {
                this.settingValues = data;
                this.career_levels = this.settingValues.career_levels.split(",") ;
                this.occupational_areas = this.settingValues.occupational_areas.split(",")  ;
                // console.log(this.career_levels) ;
                this.occupational_areas.forEach(tag => {
                    (this.profileForm.addControl(tag,new FormControl()));
                });
            },

        this.authService.user().then(
            data => {
                let currentProfile = data.json();
                this.profile = currentProfile.user;
                if (this.profile['occupational_area'] != '') {
                    for (var item of this.profile['occupational_area'].split(',')) {
                        if (this.profileForm.find(item)) {
                            console.log('Schlagwort: '+item) ;
                            (<FormControl>this.profileForm.find(item)).updateValue(1);
                        }
                    }
                }
                if(this.profile['mail_notifications'] == true){
                     (<FormControl>this.profileForm.find('mail_notifications')).updateValue(1);
                }
            }
        ).catch()
        );





        this.profileForm = formBuilder.group({
            id: ['', Validators.required],
            title: ['', Validators.nullValidator],
            firstname: ['', Validators.required],
            suffix: ['', Validators.nullValidator],
            lastname: ['', Validators.required],
            about: ['', Validators.nullValidator],
            xing: ['', CustomValidators.validateXingUrl],
            linkedin: ['', CustomValidators.validateLinkedInUrl],
            facebook: ['', CustomValidators.validateFacebookUrl],
            company: ['', Validators.required],
            company_street: ['', Validators.required],
            company_house_number: ['', Validators.required],
            company_zip: ['', Validators.required],
            company_city: ['', Validators.required],
            career_level: ['', Validators.nullValidator],
            avatar: ['', Validators.nullValidator],
            mail_notifications: [0, Validators.nullValidator],
            occupational_area: ['', Validators.nullValidator]
        })


        this.authService.user().then(
            data => {
                let currentProfile = data.json();
                this.profile = currentProfile.user;
                if (this.profile['occupational_area'] != '') {
                    for (var item of this.profile['occupational_area'].split(',')) {
                        if (this.profileForm.find(item)) {
                            console.log('Schlagwort: '+item) ;
                            (<FormControl>this.profileForm.find(item)).updateValue(1);
                        }
                    }
                }
                (<FormControl>this.profileForm.find('id')).updateValue(this.profile.id);
                (<FormControl>this.profileForm.find('title')).updateValue(this.profile.title);
                (<FormControl>this.profileForm.find('firstname')).updateValue(this.profile.firstname);
                (<FormControl>this.profileForm.find('suffix')).updateValue(this.profile.suffix);
                (<FormControl>this.profileForm.find('lastname')).updateValue(this.profile.lastname);
                (<FormControl>this.profileForm.find('about')).updateValue(this.profile.about);
                (<FormControl>this.profileForm.find('xing')).updateValue(this.profile.xing);
                (<FormControl>this.profileForm.find('linkedin')).updateValue(this.profile.linkedin);
                (<FormControl>this.profileForm.find('facebook')).updateValue(this.profile.facebook);
                (<FormControl>this.profileForm.find('company')).updateValue(this.profile.company);
                (<FormControl>this.profileForm.find('company_street')).updateValue(this.profile.company_street);
                (<FormControl>this.profileForm.find('company_house_number')).updateValue(this.profile.company_house_number);
                (<FormControl>this.profileForm.find('company_zip')).updateValue(this.profile.company_zip);
                (<FormControl>this.profileForm.find('company_city')).updateValue(this.profile.company_city);
                (<FormControl>this.profileForm.find('career_level')).updateValue(this.profile.career_level);
                this.profileForm.markAsTouched;
                this.profileForm.updateValueAndValidity(true);
                this.titleService.setTitle(this.profile.firstname + ' ' + this.profile.lastname);

            }
        ).catch();

    }

    ngOnInit() { 
        $(".sidebar_linkWrap li a").attr("disabled", "disabled");
        $('.sidebar_linkWrap li a').click(function(event) {
            event.preventDefault();
            event.stopPropagation();
            Materialize.toast('Bitte dein Profil speichern', 4000, 'red');
        });
        this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        this.profileService.findOne(this.profile.id).subscribe(
                data => {
                    this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data });
                    this.profile = data;
                    Materialize.toast('Dein Profilbild ist hochgeladen!', 4000, 'green');

                }
            )  
        };

        this.uploader.onBuildItemForm = (item, form) => {
            form.append("user", this.profile.id);
            form.append("description", 'avatar');
        };


    } 

    public enableConfirm(): void {
        this.confirmShow = true ;
        $('#copyright_confirm').prop('disabled', false);
        $('#copyright_confirm').prop('checked', false);

    }

    uploadFile() {

        this.uploader.queue.forEach(item => {
            if (item.file.size > 3000000){
                Materialize.toast('Das Bild ist zu groß, bitte wähle ein anderes Bild aus', 4000, 'red');
                this.uploader.clearQueue();
            }                
            else{
                item.upload(); 
                this.queueItem = this.uploader.getIndexOfItem(item);
            }
        });
        $('#copyright_confirm').prop('disabled',true);


    }

    checkForm(): boolean{

        if (this.profileForm.valid){
            return true; 
        }
        else return false;
    }


    onSubmit(): void {

        let new_occupational_areas = [];
        for (var item of this.occupational_areas) {
            if (this.profileForm.controls[item].value) {
                new_occupational_areas.push(item);
            }
         }
        (<FormControl>this.profileForm.find('occupational_area')).updateValue(new_occupational_areas.join());
        this.profileForm.updateValueAndValidity(true);
        if (this.profileForm.valid) {
            // Avatar validation
            if(this.profile.avatar_data['name'] == "missing.png"){
                Materialize.toast('Bitte wähle ein Profilbild aus', 4000, 'red');
            }

            else if ($('#copyright_confirm').prop('checked') == false && $('#copyright_confirm').prop('disabled') == false){
                Materialize.toast('Bitte Bestätigen Sie dass Sie alle Rechte vom hochgeladenen Bild haben', 4000, 'red');
            }  
 
            else{
            // else $('#copyright_confirm').prop('checked',true)  ;
   
                Materialize.toast('Dein Profil wird gespeichert...', 4000);
                let multipartform = new FormData();
                this.profileService.updateOne(this.profileForm.value, multipartform);
                this.profileService.findOne(this.profile.id).subscribe(
                    data => {
                        this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data });
                        this.profile = data;
                        Materialize.toast('Dein Profil wurde gespeichert', 4000, 'green');
                        this.router.navigate(['/profile/'+this.profile.id]);

                    }
                )
            }
        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }
    }

}

