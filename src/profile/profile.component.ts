import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import { MaterializeDirective } from "angular2-materialize";
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';

import { AppState, IQCEvent, IQCUserProfile, IQCEventItems, IQCBookmarks } from '../shared/interfaces'
import { SETEVENT, SETEVENTITEMS,  DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions'
import { IMAGE_URL, SERVER_PUBLIC_URL } from '../config';
import { SERVER_URL } from '../config';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { EventUserIdSearch, FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventIsInList} from '../shared/pipe.filters'
import { DateFormatPipe } from '../../node_modules/angular2-moment/DateFormatPipe';
import { ProfileService } from '../shared/profile.service';
import { EventService } from '../shared/event.service';
import { AuthGuardService } from '../shared/auth.guard.service';

@Component({
    selector: 'div',
    template: require('./profile.html'),
    directives: [ROUTER_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    providers: [ProfileService, AuthGuardService],
    pipes: [EventIsInList, EventUserIdSearch, FilterPipe, CapitalizePipe, DateFormatPipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch]
})

export class Profile {

    public qcuserprofile: Observable<IQCUserProfile>;
    public qceventitems: Observable<IQCEventItems>;
    public currentuser;
    private currentUserLoggedIn:Boolean = false;
    public events_error: Boolean = false;
    public qceventbookmarks: Observable<IQCBookmarks>;
    public currentbookmarks;
    private IMAGE_URL: string = IMAGE_URL;
    private SERVER_PUBLIC_URL: string = SERVER_PUBLIC_URL;
    private SERVER_URL = SERVER_URL;
    public currentUserIsOwner: boolean = false;


    constructor(
        private titleService: Title,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private profileService: ProfileService,
        private eventService: EventService
    ) {

        //Find Future Events
        this.eventService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETEVENTITEMS, payload: data });;
                // this.futureEvents = data ; 
            },
            err => { console.log(err) }
        );



        this.qceventitems = store.select<IQCEventItems>('qceventitems');
        this.qcuserprofile = store.select<IQCUserProfile>('qcuserprofile');
        this.route.params.subscribe(params => {

            let id:number = params['id'];
            this.profileService.findOne(id).subscribe(
                data => {
                    this.currentuser = data;
                    this.titleService.setTitle(this.currentuser.firstname + ' ' + this.currentuser.lastname);
                },
                err => {
                    this.events_error = true
                }
            );

            this.qcuserprofile.subscribe(
                data => {
                    if (data.id == id ) {
                        this.currentUserLoggedIn = true;
                    }
                }
            )
        });
    }

    delBookmark(event: IQCEvent) {

        document.getElementById(event.id.toString()).remove();
        this.profileService.delBookmark(this.currentuser.id, event.id);
    }

    // Change to default pic if the source is broken 
    updateUrlAvatar(event) { 
        var target = event.target || event.srcElement || event.currentTarget;
        $(target).attr('src',SERVER_PUBLIC_URL+'/img/default/missing.png')
    }

    updateUrlEvent(event,type) { 
        var target = event.target || event.srcElement || event.currentTarget;
        if(type=="workshop" || type=="lab"){
             $(target).attr('src',SERVER_PUBLIC_URL+'/img/default/missing-talk.jpg');
        }
        if(type=="talk"){
             $(target).attr('src',SERVER_PUBLIC_URL+'/img/default/missing-talk.jpg');
        }
        else $(target).attr('src',SERVER_PUBLIC_URL+'/img/default/missing-freizeit.jpg');
            
    }




}