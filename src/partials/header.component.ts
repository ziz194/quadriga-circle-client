import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'qcheader',
    template: require('./header.html'),
    directives: [ROUTER_DIRECTIVES]
})

export class Header {

}



// Search Bar

// <nav>
//     <div class="nav-wrapper">
//       <form>
//         <div class="input-field">
//           <input id="search" type="search" required>
//           <label for="search"><i class="material-icons">search</i></label>
//           <i class="material-icons">close</i>
//         </div>
//       </form>
//     </div>
//   </nav>`,