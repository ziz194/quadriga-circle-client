import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';

@Component({
  selector: 'qc-app',
  template: require('./welcome.html'),
  directives: [ROUTER_DIRECTIVES, Sidebar, Footer, Header]
})
export class Welcome { }