declare var app: {
    environment : string
};

interface WebpackRequire {
  (id: string): any;
  context(dir: string, useSubdirs: boolean, pattern: RegExp): any;
      <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void): void;
    ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void) => void;
}

interface NodeRequire extends WebpackRequire {}

declare var require: NodeRequire
