import { Component, OnInit} from '@angular/core';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Observable}  from 'rxjs/Rx';
import { AppState, IQCUserProfile } from '../shared/interfaces'
import { SERVER_URL } from '../config';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { CustomValidators } from '../shared/validators';
import { MaterializeDirective } from "angular2-materialize";
import { ProfileService } from '../shared/profile.service';
import { ContactService } from '../shared/contact.service';
import { Store } from '@ngrx/store';
import { Title } from '@angular/platform-browser';
import { SITE_TITLE } from '../config';


@Component({
    selector: 'content',
    template: require('./contact.html'),
    directives: [REACTIVE_FORM_DIRECTIVES, Sidebar, Footer, Header, MaterializeDirective],
    providers: [ProfileService, CustomValidators, ContactService]
})

export class Contact implements OnInit {

    private qcuserprofile: Observable<IQCUserProfile>;
    private profile: IQCUserProfile;

    private result;
    private isSent: boolean = false;
    private isError: boolean = false;
    private errorMessage: string = 'Ein Fehler ist aufgetreten';
    private contactForm: FormGroup;
    private showCaptcha: boolean = true;
    private CAPTCHA_URL = SERVER_URL + 'contact/captcha';

    ngOnInit() {
        this.isSent = false;
        this.isError = false;
    }


    constructor(
        private http: Http,
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public profileService: ProfileService,
        private customValidators: CustomValidators,
        private contactService: ContactService,
        private titleService: Title

    ) {
        this.titleService.setTitle(SITE_TITLE);
        this.qcuserprofile = store.select<IQCUserProfile>('qcuserprofile');
        this.qcuserprofile.subscribe(
            data => {
                this.profile = data
            }
        );

        if (this.profile != null && this.profile.id) {
            this.showCaptcha = false;
            this.contactForm = formBuilder.group({
                name: [this.profile.firstname + ' ' + this.profile.lastname, Validators.compose([Validators.required, Validators.minLength(4)])],
                phone: ["", Validators.nullValidator],
                email: [this.profile.email, Validators.compose([Validators.required])],
                message: ["", Validators.required],
             });
        } else {
            this.showCaptcha = true;
            this.contactForm = formBuilder.group({
                name: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
                phone: ['', Validators.nullValidator],
                email: ['', Validators.compose([Validators.required, CustomValidators.validateEmail ])],
                message: ['', Validators.required],
                captcha: ['', Validators.required]
            });
      
      }
    }

    onSubmit(): void {

        if (this.contactForm.valid) {
            this.contactService.sendRequest(this.contactForm.value).then(

                response => {
                    this.result;
                    this.isSent = true;
                    Materialize.toast('Deine Nachricht wurde verschickt.', 4000, 'green');
                }

            ).catch(
                error => {
                    this.isError = true;
                    this.isSent = false;
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error.json().errors , 4000, 'red');
                }

                );
        } else {
            // this.isError = true;
        }

    }


    changeCaptcha():void{

        this.CAPTCHA_URL =  this.CAPTCHA_URL + '?' + Date.now().toString();

    }

}