import { Component } from '@angular/core';
import { SITE_TITLE } from '../config';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState,IQCCommentItems, IQCUserProfile, IQCEventItems, IQCEvent } from '../shared/interfaces';
import { SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL, ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions';
import { DateFormatPipe } from '../../node_modules/angular2-moment/DateFormatPipe';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch } from '../shared/pipe.filters'
import { Title } from '@angular/platform-browser';
import { IMAGE_URL } from '../config'
import { ContactService } from '../shared/contact.service';
import { CustomValidators } from '../shared/validators';
import { FormControl, FormArray, FormGroup, REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { SERVER_URL } from '../config';


@Component({
    selector: 'content',
    template: require('./home.html'),
    directives: [ROUTER_DIRECTIVES, REACTIVE_FORM_DIRECTIVES],
    pipes: [FilterPipe, CapitalizePipe, DateFormatPipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch],
    providers: [CustomValidators, ContactService]
})

export class Home {

    public qceventitems: Observable<IQCEventItems>;
    public qcuserprofile: Observable<IQCUserProfile>;
    private IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;
    private isSent: boolean = false;
    private isError: boolean = false;
    private contactForm: FormGroup;
    private showCaptcha: boolean = true;
    private CAPTCHA_URL = SERVER_URL + 'contact/captcha';


    constructor(public store: Store<AppState>,
        private router: Router,
        private location: Location,
        private titleService: Title,
        private customValidators: CustomValidators,
        private formBuilder: FormBuilder,
        private contactService: ContactService
        


    ) {
        this.titleService.setTitle(SITE_TITLE);
        this.qceventitems = store.select<IQCEventItems>('qceventitems');
        this.qcuserprofile = store.select<IQCUserProfile>('qcuserprofile');

        this.isSent = false;
        this.isError = false;

        this.showCaptcha = true;
        this.contactForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required, CustomValidators.validateEmail])],
            message: ['', Validators.required],
            captcha: ['', Validators.required]
        });

    }

    toggleBookmark(event: IQCEvent) {

        if (event.isBookmarked) {
            this.store.dispatch({ type: DEL_EVENT_BOOKMARK, payload: event.id });
        } else {
            this.store.dispatch({ type: ADD_EVENT_BOOKMARK, payload: event.id });
        }
        event.isBookmarked = !event.isBookmarked;
        this.store.dispatch({ type: UPDATE_EVENTITEM, payload: event });

    }

    onSubmit(): void {

        if (this.contactForm.valid) {
            this.contactService.sendRequest(this.contactForm.value).then(

                response => {
                    this.isSent = true;
                    Materialize.toast('Deine Nachricht wurde verschickt.', 4000, 'green');
                    (<FormControl>this.contactForm.find('message')).updateValue('');
                    (<FormControl>this.contactForm.find('captcha')).updateValue('');
                    this.CAPTCHA_URL = this.CAPTCHA_URL + '?' + Date.now().toString();
            }

            ).catch(
                error => {
                    this.CAPTCHA_URL = this.CAPTCHA_URL + '?' + Date.now().toString();
                    this.isError = true;
                    this.isSent = false;
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error.json().errors, 4000, 'red');
                }

                );
        }

    }

    changeCaptcha(): void {

        this.CAPTCHA_URL = this.CAPTCHA_URL + '?' + Date.now().toString();

    }
}