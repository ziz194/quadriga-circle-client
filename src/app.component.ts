import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Route, ActivatedRoute, provideRouter } from '@angular/router';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { Angulartics2GoogleAnalytics } from 'angulartics2/src/providers/angulartics2-google-analytics';
import { Angulartics2 } from 'angulartics2';
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Store } from '@ngrx/store';
import { INCREMENT, DECREMENT, RESET, SETVAL, SETEVENTITEMS, SETPASTEVENTITEMS, SETUSERPROFILEITEMS, SETALLEVENTITEMS, SET_PROFILE_ITEMS } from './shared/state.actions';
import { EventService } from './shared/event.service';
import { ProfileService } from './shared/profile.service';
import { FAQService } from './shared/faq.service';
import { AuthService } from './shared/auth.service';
import { SettingsService } from './shared/setting.service';

import { AppState, IQCEvent, IQCEventItems, IQCUserProfileItems, IQCFaqItems, IQCFaq } from './shared/interfaces';
import { Sidebar } from './sidebar/sidebar.component';
import { LoginForm } from './login/login.component';
import { RegisterForm } from './login/register.component';
import { PasswordForm } from './login/password.component';
import { EventList } from './event/eventlist.component';
import { EventDetail } from './event/eventdetail.component';
import { EventEdit } from './event/eventedit.component';
import { EventNew } from './event/eventnew.component';
import { EventProsposal } from './event/eventprosposal.component';
import { MarketplaceList } from './event/marketplacelist.component';
import { Faq } from './faq/faq.component';
import { Info } from './info/info.component';
import { Settings } from './settings/settings.component';
import { Imprint } from './imprint/imprint.component';
import { Home } from './home/home.component';
import { ProfileList } from './profile/profilelist.component';
import { Profile } from './profile/profile.component';
import { ProfileEdit } from './profile/profileedit.component';
import { Contact } from './contact/contact.component';
import { Welcome } from './welcome/welcome.component';
import { AuthGuardService } from './shared/auth.guard.service';
import { CanActivate } from '@angular/router';



export const routes = [
    { path: '', component: Home },
    { path: 'login', component: LoginForm },
    { path: 'register', component: RegisterForm },
    { path: 'password', component: PasswordForm },
    { path: 'password/reset/:token', component: PasswordForm },
    { path: 'profile/edit', component: ProfileEdit, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'profile/:id', component: Profile, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'profile', component: Profile, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'profiles', component: ProfileList, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'events/new', component: EventNew, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'events/prosposal', component: EventProsposal, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'events/:id/edit' , component: EventEdit, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'events/:id' , component: EventDetail, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'events', component: EventList, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'marketplace', component: MarketplaceList, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'faq', component: Faq },
    { path: 'info', component: Info },
    { path: 'settings', component: Settings, pathMatch: 'full', canActivate: [AuthGuardService]},
    { path: 'imprint', component: Imprint},
    { path: 'contact', component: Contact },
    { path: 'welcome', component: Welcome },
    { path: '**', redirectTo: '/', pathMatch: 'full'}


];

@Component({
    selector: 'qc-app',
    template: '<router-outlet></router-outlet>',
    providers: [Angulartics2GoogleAnalytics, EventService, ProfileService, FAQService, AuthGuardService, SettingsService],
    directives: [ROUTER_DIRECTIVES]
})


export class AppComponent implements OnInit {

    public counter: Observable<number>;
    public event: Observable<IQCEvent>;
    public qceventitems: Observable<IQCEventItems>;
    public qceventitems_error: Boolean = false;
    public qcprofileitems: Observable<IQCUserProfileItems>;
    public qcfaqitems: Observable<IQCFaqItems>;
    
    constructor(
        public store: Store<AppState>,
        public profileService: ProfileService,
        public eventService: EventService,
        public SettingsService: SettingsService,
        public faqService: FAQService) {
        this.counter = store.select<number>('counter');
        this.event = store.select<IQCEvent>('qcevent');
        this.qceventitems = store.select<IQCEventItems>('qceventitems');
        this.qcprofileitems = store.select<IQCUserProfileItems>('qcprofileitems');
        this.qcfaqitems = store.select<IQCFaqItems>('qcfaqitems');
      //  angulartics2: Angulartics2;
      //  angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics
    }

    ngOnInit() {
       
        this.eventService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETEVENTITEMS, payload: data });;
            },
            err => { this.qceventitems_error = true }
        );   

            this.profileService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETUSERPROFILEITEMS, payload: data });;
            },
            err => { console.log('error fetch') }
        );   

    

    }

}
