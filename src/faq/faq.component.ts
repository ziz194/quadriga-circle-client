import { Component } from '@angular/core';
import { SERVER_URL } from '../config';
import { SITE_TITLE } from '../config';
import { FAQService } from '../shared/faq.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, provideRouter } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location, NgClass } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState, IQCFaqItems, IQCFaq  } from '../shared/interfaces';
import { Sidebar } from '../sidebar/sidebar.component';
import { Footer } from '../partials/footer.component';
import { Header } from '../partials/header.component';
import { MaterializeDirective } from "angular2-materialize";
import { Title } from '@angular/platform-browser';


@Component({
    selector: 'div',
    template: require('./faq.html'),
    providers: [FAQService],
    directives: [ROUTER_DIRECTIVES, Sidebar, Footer, MaterializeDirective]
})

export class Faq {

    public qcfaqitems;

    constructor(public store: Store<AppState>,
        public faqService: FAQService,
        private titleService: Title
    ) {
        this.titleService.setTitle(SITE_TITLE);
        this.faqService.findAll().then(
            result => {
                let results = result.json()
                this.qcfaqitems = results.data;
            }
        ).catch(
            error => {
                return false;
            }
            );

    }

}