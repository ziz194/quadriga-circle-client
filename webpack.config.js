var webpack = require('webpack');
var HtmlWebpackPluign = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var webpackUglifyJsPlugin = require('webpack-uglify-js-plugin');

module.exports = {

  entry: './src/main.ts',
  output: {
    path: './dist',
    filename: 'app.bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.ts'],
    alias: {
      materializecss: 'materialize-css/dist/css/materialize.css',
      materialize: 'materialize-css/dist/js/materialize.js',
    }
  },
  devtool: "source-map",
  module: {
    loaders: [
      { test: /\.ts?$/, loader: 'ts-loader' },
      { test: /\.html$/, loader: "raw-loader", exclude: '/node_modules' },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css?sourceMap!sass?sourceMap') },
      { test: /\.svg$/, loader: 'url?limit=65000&mimetype=image/svg+xml&name=fonts/[name].[ext]' },
      { test: /\.woff$/, loader: 'url?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]' },
      { test: /\.woff2$/, loader: 'url?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'url?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]' },
      { test: /\.eot$/, loader: 'url?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]' },
      { test: /\.css/, loader: "style-loader!css-loader&name=css/[name].[ext]" },
      { test: /\.png/, loader: "url-loader?limit=100000&minetype=image/png&name=img/[name].[ext]" },
      { test: /\.jpg/, loader: "file-loader" }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      minimize: false,
      sourceMap: true,
      output: {
        comments: true
      },
      compressor: {
        warnings: false
      }
    }),
    new HtmlWebpackPluign({
      template: './src/index.html'
    }),
    new ExtractTextPlugin('style.css'),
    new CopyWebpackPlugin([
      { from: 'assets/img/', to: 'img' },
      { from: '.htaccess' }]),
    new webpack.DefinePlugin({
      app: {
        environment: JSON.stringify(process.env.APP_ENVIRONMENT || 'development')
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '\'' + process.env.NODE_ENV + '\'',
      },
    }),

  ],
  devServer: {
    contentBase: "/dist/",
    hot: true,
    port: 3000,
    historyApiFallback: true
  },
  externals: {
    jquery: 'jQuery'
  }
};
