module.exports = function (config) {

    config.set({
        frameworks: ['jasmine'],
        files: [{ pattern: './src/specs.ts', watched: false }],
        preprocessors: {
            './src/specs.ts': ['webpack']
        },
        webpackMiddleware:{
            stats:'errors-only',
        },
        webpack: {
            resolve: {
                extensions: ['', '.js', '.ts'],
                alias: {
                    materializecss: 'materialize-css/dist/css/materialize.css',
                    materialize: 'materialize-css/dist/js/materialize.js',
                }
            },
            module: {
                loaders: [
                    { test: /\.ts?$/, loader: 'ts-loader' },
                    { test: /\.html$/, loader: "raw-loader", exclude: '/node_modules' }
                ]
            }
        },
        browsers:['Chrome'] 
        
    })

};
